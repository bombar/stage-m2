# Readme for the package nicematrix

Author: F. Pantigny (`fpantigny@wanadoo.fr`).

CTAN page: `https://ctan.org/pkg/nicematrix`

## License
The LaTeX extension `nicematrix` is distributed under the LPPL 1.3 license.

## Presentation

The LaTeX package `nicematrix` provides new environments similar to the classical environments
`{array}` and `{matrix}` but with some additional features. Among these features are the
possibilities to fix the width of the columns, to draw continuous ellipsis dots between the cells
of the array and to add exterior rows and columns (so called *border matrices*).


## Installation

The package `nicematrix` is present in the distributions MiKTeX, TeXLive and MacTeX.

For a manual installation:

* put the files `nicematrix.ins` and `nicematrix.dtx` in the same directory; 
* run `latex nicematrix.ins`.

The file `nicematrix.sty` will be generated.

The file `nicematrix.sty` is the only file necessary to use the extension `nicematrix`.

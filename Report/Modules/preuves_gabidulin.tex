\begin{thm}[Interpolation de Lagrange linéaire (\ref{lagrange})]\label{preuve lagrange}
	Soit $(g_1, \dots, g_n) \in \Fqm^n$ de rang $n$. Soit $(y_1, \dots, y_n) \in \Fqm^n$. Il existe un unique $q$-polynôme $P$ de $q$-degré au plus $n-1$ tel que pour tout $i$, $P(g_i) = y_i$. Par ailleurs, on peut calculer $P$ en $O(n^3 + n^2\log(m))$ opérations dans $\Fqm$.
\end{thm}
\begin{proof}

	~\begin{itemize}
		\item \textbf{Existence:} Soit $\tilde{P}_i$ l'unique $q$-polynôme unitaire de $q$-degré $n-1$ dont l'ensemble des racines est $V_i := \Vect(g_j\mid j\neq i)$. Alors $\tilde{P}_i(g_i) \neq 0$. Posons $P_i := \tilde{P}_i(g_i)^{-1}\tilde{P}_i.$

		      La famille ainsi construite vérifie $P_i(g_j) = \delta_{i,j}$, et chaque $P_i$ peut être construit en $O(n^2 + n\log(m))$ opérations~(\cite{muratResultantsOrePolynomials2014}). On en déduit que le $q$-polynôme suivant convient
		      \[
			      P := \sum_{i=1}^n y_i P_i.
		      \]

		\item \textbf{Unicité:} Si $P$ et $Q$ vérifient tous les deux les hypothèses, alors $P-Q$ est un polynôme linéaire de $q$-degré au plus $n-1$ s'annulant sur tout $V = \Vect(g_1, \dots, g_n)$. Par liberté des $g_i$, $V$ est de dimension $n$. Le Théorème~\ref{racines d'un q-polynome} permet alors d'affirmer que $P-Q = 0$ ou encore $P = Q$.
	\end{itemize}

\end{proof}

\begin{prop}[Équivalence entre reconstruction de $q$-polynômes et décodage \eqref{equivalence rpl-decodage}.]\label{preuve equivalence rpl-decodage}
	~\begin{itemize}
		\item Si $(V, f)$ est une solution du problème de reconstruction $\mathbf{RPL}(\mathbf{y}, \mathbf{g}, k, t)$, alors $c := f(\mathbf{g})$ est une solution du problème de décodage $\mathbf{Decodage}(\Gab_{k}(\mathbf{g}), \mathbf{y}, t)$.
		\item Réciproquement, si $\mathbf{c} = f(\mathbf{g})$ est une solution de $\mathbf{Decodage}(\Gab_{k}(\mathbf{g}), \mathbf{y}, t)$ alors on peut trouver en temps polynômial un $q$-polynôme $V$ de $q$-degré au plus $t$ tel que $(V, f)$ soit une solution de $\mathbf{RPL}(\mathbf{y}, \mathbf{g}, k, t)$.
	\end{itemize}
\end{prop}

\begin{proof}
	Soit $(\mathbf{y}, \mathbf{g}, k, t)$ une instance de $\mathbf{RPL}$. Considérons le code de Gabidulin $\mathcal{G} = Gab_k(\mathbf{g})$. Alors $(\mathcal{G}, \mathbf{y}, t)$ est une instance du problème de décodage.

	\begin{itemize}
		\item Soit $(V,f)$ une solution du problème de reconstruction. Posons $c_i := f(g_i)$. Alors $\mathbf{c} \in Gab_k(\mathbf{g})$ puisque $f$ est de $q$-degré $<k$. Par ailleurs $V(y_i) = V(f(g_i))$ pour tout $i$ donc par linéarité $V(y_i - f(g_i)) = 0.$ Posons $e_i := y_i - f(g_i)$. Alors le rang de $\mathbf{e}$ est inférieur au $q$-degré de $V$, donc $e$ est de rang au plus $t$. Enfin on a bien par construction

		      \[
			      \mathbf{y} = \mathbf{c} + \mathbf{e}
		      \]
		      donc $\mathbf{c}$ est une solution au problème de décodage et est obtenu par simple évaluation de $f$.

		\item Réciproquement, soit $\mathbf{c}\in \mathcal{G}$ une solution au problème de décodage. Il existe donc $\mathbf{e}$ de rang au plus $t$ tel que $\mathbf{y} = \mathbf{c} + \mathbf{e}$. Soit $E := \Vect(e_1, \dots, e_n)$. Par hypothèse, $E$ est de dimension $d\le t$. D'après le Théorème~\ref{interpolation}, on peut construire en temps polynomial un $q$-polynôme $V$ de $q$-degré $d\le t$ et tel que $E$ soit exactement l'ensemble des racines de $V$. Par ailleurs on dispose d'un $q$-polynôme $f$ de degré $<k$ et tel que $c_i = f(g_i)$. Ce dernier peut être calculé en temps polynômial grâce au Théorème~\ref{lagrange}. Enfin on a pour tout $i$ :
		      \[
			      V(y_i) = V(c_i) + V(e_i) = V(c_i) = V(f(g_i))
		      \]
		      donc $(V, f)$ est une solution du problème de reconstruction.
	\end{itemize}
\end{proof}

\newpage

\begin{thm}[Correction de l'algorithme de décodage \eqref{decodage WBL}]\label{preuve decodage WBL}
	Si $t \le (n-k)/2$ et si $\mathbf{e}$ est de rang $\le t$ et vérifie $\mathbf{y} = f(\mathbf{g}) + \mathbf{e}$ alors pour toute solution $(V, N) \neq (0,0)$ du système~\eqref{WBL} on a $N = V\circ f$.
\end{thm}

\begin{proof}
	On rappelle le système~\eqref{WBL} :

	\begin{align*}
		\left\lbrace \begin{array}{l}
			V(y_i) = N (g_i), \quad i=1,\dots, n \\
			\deg_q(V) \le t                      \\
			\deg_q(N)\le k + t - 1.              \\
		\end{array} \right.
	\end{align*}

	Soit $(V,N)$ une solution non nulle. Par hypothèse on a pour tout $i$, $N(g_{i}) = (V\circ f)(g_{i}) + V(e_{i})$, ou encore

	\[
		(N - V\circ f)(g_{i}) = V(e_{i}).
	\]

	Posons $R := N - V\circ f$ et supposons par l'absurde que $R \neq 0$.


	\begin{enumerate}[label=(\roman*)]
		\item Notons $Q$ la restriction de $R$ à l'espace de dimension $n$ engendré par $\mathbf{g}$. $R$ est de $q$-degré au plus $k+t-1$ et par conséquent le noyau de $Q$ est de dimension au plus $k+t-1$. Le théorème du rang nous donne donc

		      \[
			      \dim \Vect(R(g_{1}), \dots, R(g_{n})) = \rk(Q) = n - \dim \Ker Q \ge n - (k+t-1).
		      \]

		\item $\mathbf{e}$ est de rang $t$ donc
		      \[
			      \rk(V(e_1), \dots, V(e_{n})) \le t.
		      \]
	\end{enumerate}
	Ces deux quantités étant égales, on en déduit que $t > \frac{n-k}{2}$, ce qui contredit l'hypothèse. Par conséquent, $R = 0$ et donc $N = V\circ f$.
\end{proof}

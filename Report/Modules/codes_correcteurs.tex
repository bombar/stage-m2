\subsection{Transmission, erreurs et correction}

La numérisation de l'information a été un tournant majeur de l'époque moderne. Elle offre la possibilité de conserver l'information de façon illimitée. En revanche, la transmission de cette information doit prendre en compte les erreurs, les détecter, voire les corriger. La numérisation de l'information offre aussi cette possibilité.

L'idée est la suivante : on souhaite transmettre un message $m$ entre un \emph{émetteur} et un \emph{récepteur}. Ce message emprunte alors un \emph{canal} de communication. Ce dernier est susceptible d'introduire des erreurs. On va donc \emph{encoder} l'information de sorte qu'en sortie du canal il soit possible de corriger les erreurs lorsqu'elles ne sont pas trop nombreuses. Un encodage est une fonction injective de l'espace des messages vers un nouvel espace, le code correcteur $\mathcal{C}$. Dans la suite on ne considérera que des codes linéaires, c'est-à-dire que $\mathcal{C}$ sera un espace vectoriel, et la fonction d'encodage sera linéaire. Pour les applications, on considère en général des codes correcteurs définis sur un corps fini $\Fq$. Un code correcteur de longueur $n$ et de dimension $k$ est alors un sous-espace vectoriel de $\Fq^{n}$ de dimension $k$. Intuitivement, l'écart entre $n$ et $k$ représente la redondance que l'on introduit pour permettre de corriger les erreurs. Plus le rapport $\frac{n}{k}$ est proche de $0$, plus on pourra espérer corriger d'erreurs, mais plus le coût de transmission sera important. Dans cette modélisation, un mot bruité est de la forme $y := c+e$ où $c$ est un mot de code et $e$ est l'erreur introduite par le canal. Corriger l'erreur consiste à retrouver $c$ à partir de la connaissance de $y$. Lorsque l'erreur est \emph{suffisamment petite}, cela revient à retrouver l'élément de $\mathcal{C}$ le \emph{plus proche} de $y$.


\begin{figure}[H]
	\includegraphics[width=0.9\textwidth]{Logo/canal.jpg}
	\caption{Correction d'erreurs}
\end{figure}


Pour mesurer cette proximité entre le mot bruité et le mot de code, on définit une distance sur notre espace. Dans le cas de codes linéaires, on pourra aussi introduire une notion de \emph{poids}. Le poids d'un mot (ou d'une erreur) va être sa distance au mot nul. On dira aussi qu'un mot bruité par une erreur de poids $t$ possède $t$ erreurs. La distance minimale entre deux mots distincts du code va permettre de quantifier le nombre maximal d'erreurs que l'on peut corriger. En effet, pour un code de distance minimale $d$, les boules fermées de rayon $\lfloor \frac{d-1}{2}\rfloor$ centrées en un mot du code sont deux à deux disjointes. Cela signifie que si un mot reçu a au plus $\lfloor \frac{d-1}{2} \rfloor$ erreurs, alors il existe un \emph{unique} mot de code qui lui correspond. Cette borne est appelée \emph{rayon de décodage unique} ou \emph{capacité de correction} du code. Autrement dit, pour obtenir un code correcteur efficace, on veut maximiser la distance minimale tout en conservant le rapport $\frac{k}{n}$ proche de $1$. Plusieurs distances sont évidemment envisageables, tout dépend des applications. La distance la plus naturelle que l'on puisse introduire pour mesurer l'écart entre deux mots est la distance de Hamming. Cependant, dans ce rapport, nous nous intéresserons à une autre métrique : la métrique rang. Pour ces deux métriques, la distance minimale peut-être reliée à la longueur et à la dimension du code par l'\emph{inégalité de Singleton} :

\begin{defi}[Inégalité de Singleton]
	Soit $\mathcal{C}$ un code linéaire de longueur $n$ et de dimension $k$, en métrique de Hamming, ou en métrique rang. On note $d$ sa distance minimale. Alors $d \le n - k + 1.$
\end{defi}
La capacité de correction d'un code de longueur $n$ et de dimension $k$ atteignant la borne de Singleton vaut donc $\lfloor \frac{n-k}{2}\rfloor$. Un tel code est dit MDS (\emph{Maximum Distance Separable}) en métrique de Hamming, et MRD (\emph{Maximum Rank Distance}) en métrique rang.

Terminons ce paragraphe en mentionnant les deux façons \emph{canoniques} de définir un code (linéaire) :

\begin{defi}[Matrice génératrice]
	Soit $\mathcal{C}$ un code de longueur $n$ et de dimension $k$. Une \emph{matrice génératrice} de $\mathcal{C}$ est une matrice $\mathbf{G} \in \Fq^{k\times n}$ dont les lignes forment une famille génératrice de $\mathcal{C}$. Autrement dit,

	\[
		\mathcal{C} = \{ \mathbf{mG} \mid \mathbf{m}\in \Fq^{k}\}.
	\]
\end{defi}

\begin{defi}[Matrice de parité]
	Soit $\mathcal{C}$ un code de longueur $n$ et de dimension $k$. Une \emph{matrice de parité} de $\mathcal{C}$ est une matrice $\mathbf{H} \in \Fq^{(n-k)\times n}$ dont le noyau (à droite) est égal à $\mathcal{C}$ \ie:

	\[
		\mathcal{C} = \{ \mathbf{x} \in \Fq^{n} \mid \mathbf{Hx^{T}} = 0 \}.
	\]
\end{defi}


\subsection{Codes matriciels et métrique rang}

Dans toute la suite, $q$ désignera une puissance d'un nombre premier, et $\Fq$ sera le corps fini à $q$ éléments.

Considérons l'espace $\Fq^{m\times n}$ des matrices à $m$ lignes et $n$ colonnes à coefficients dans $\Fq$ et posons pour toutes matrices $X$ et $Y$
\[
	d(X, Y) := \rk(X-Y).
\]
Le \emph{poids} d'une matrice sera alors son \emph{rang}. On vérifie aisément la propriété suivante :
\begin{prop}
	$d(\cdot,\cdot)$ munit $\Fq^{m\times n}$ d'une structure d'espace métrique.
\end{prop}
On appelle alors \emph{code matriciel} de longueur $m\times n$ et de dimension $k$ tout sous-espace vectoriel de $\Fq^{m\times n}$ de dimension $k$. Un tel code sera appelé un $[m\times n, k]$ code (matriciel).

\noindent De façon analogue à ce qui se fait en métrique de Hamming, on peut définir une notion de support en métrique rang :

\begin{defi}[Support d'une matrice]
	~\begin{itemize}
	  \item Pour $X\in \Fq^{m\times n}$ on appelle \emph{support colonne} ou tout simplement \emph{support} l'espace engendré par ses colonnes : $\Supp(X) := \Vect(X_{1}, \dots, X_{n}) = \{Xy^{T}\mid y\in \Fq^{n}\}$.
	  \item De la même façon, on appelle \emph{support ligne} l'espace engendré par ses lignes : $\Rsupp(X) := \left\{yX \mid y\in \Fq^{m} \right\}.$
	\end{itemize}

	On a alors $\rk(X) = \dim\Supp(X) = \dim\Rsupp(X).$
\end{defi}



Parmi les codes matriciels, certains peuvent être décrits de manière bien plus compacte: ce sont les codes $\Fqm$-linéaires.

\subsection{Codes \texorpdfstring{$\Fqm$}{Fqm}-linéaires}

$\Fqm$ est une extension algébrique de degré $m$ de $\Fq$. Par suite, il est naturellement muni d'une structure de $\Fq$-espace vectoriel de dimension $m$. Considérons $\mathcal{B} =(\beta_{1}, \dots, \beta_{m})$ une $\Fq$-base. Tout élément $x\in\Fqm^{n}$ peut alors être décrit par une matrice $X\in\Fq^{m\times n}$ où les colonnes de $X$ sont les $n$ composantes de $x$ décomposées dans la base $\mathcal{B}$.

Plus formellement, on définit un opérateur d'extension :

\[
	\ext_{\mathcal{B}} : \left\{
	\begin{array}{ccc}
		\Fqm^{n}                   & \rightarrow & \Fq^{m\times n} \\
		x := (x_{1}, \dots, x_{n}) & \mapsto     & X :=
		\begin{bmatrix}
			x_{1,1}  & \dots  & x_{n,1} \\
			\vdots   & \ddots & \vdots  \\
			x_{1, m} & \dots  & x_{n,m}
		\end{bmatrix}.                                 \\
	\end{array}\right.
\]

À tout couple (code $\Fqm$-linéaire, base de $\Fqm/\Fq$) on peut alors associer un code matriciel :


\begin{defi}[Code matriciel associé à un code $\Fqm$-linéaire] \label{poids en metrique rang Fqm-lineaire} \ \\
	Soit $\mathcal{C}$ un $[n, k]$-code $\Fqm$-linéaire, c'est-à-dire un $\Fqm$-sous-espace vectoriel de $\Fqm^{n}$ de dimension $k$. Soit $\mathcal{B}$ une $\Fq$-base de $\Fqm$. L'espace $\ext_{\mathcal{B}}(\mathcal{C})$ est alors un $[m\times n, m\times k]$ code matriciel sur $\Fq$ et le \emph{poids} de $c\in \mathcal{C}$ est défini comme le rang de son extension dans la base $\mathcal{B}$.
\end{defi}

\begin{rem}
	La représentation matricielle d'un vecteur dépend évidemment de la base de l'extension choisie, mais pas son poids. Dans la suite, sauf exception, on ne mentionnera pas la base que l'on choisit (quitte à considérer une base normale de l'extension par exemple au moment de l'implémentation).
\end{rem}


On peut alors définir le \emph{support} d'un mot de code de façon naturelle :

\begin{defi}[Support en métrique rang] \label{support en metrique rang}\ \\
	Soit $x\in\Fqm^{n}$. On appelle \emph{support} de $x$ le support colonne de $\ext(x)$ pour une base prédéfinie. De même, on appellera \emph{support ligne} de $x$ le support ligne de la matrice associée.
\end{defi}


En cryptographie, toutes les propositions à base de codes en métrique rang utilisent des codes $\Fqm$-linéaires. Ils ont l'avantage de permettre de réduire la taille des clés d'un facteur de l'ordre de $m$ par rapport à un code matriciel général.

\subsection{Chiffrement à base de codes correcteurs}

En 1978, McEliece propose un système de chiffrement asymétrique à l'aide de codes correcteurs. Son idée est la suivante: un code linéaire $\mathcal{C}$ spécifié par une matrice génératrice $\mathbf{G}$ est publié. Le détenteur de la clé privée connaît une structure secrète de $\mathcal{C}$ qui lui permet de décoder tout mot bruité par une erreur de poids inférieur à une certaine valeur notée $w_{pub}$ qui est aussi publique. Le chiffré d'un message $\mathbf{m}$ est alors $\mathbf{c} = \mathbf{mG} + \mathbf{e}$ où $\mathbf{e}$ est une erreur de poids $w_{pub}$. Le déchiffrement consiste simplement à décoder ce mot bruité à l'aide de la structure secrète.

La sécurité de ce système repose sur le problème du décodage générique, parfois appelé comme en anglais \emph{Syndrome Decoding} et noté \emph{SD}, ou \emph{RSD} en métrique rang. Il est souvent défini dans la littérature à l'aide d'une matrice de parité. On en donne ici une version équivalente avec matrice génératrice :

\begin{problem}[Décodage générique]\ \\
Étant donnés $\mathbf{G}\in\Fq^{k\times n}, w\in\Nstar, \mathbf{y}\in \Fq^{n}$, trouver $\mathbf{e}$ de poids exactement $w$ tel que $\mathbf{y}-\mathbf{e} = \mathbf{mG}$ pour un certain $\mathbf{m}\in\Fq^{k}$.
\end{problem}


Afin d'analyser sa complexité, il est usuel de considérer une variante décisionnelle du problème :

\begin{problem}[Décodage générique (Version décisionnelle)]\ \\
Étant donnés $\mathbf{G}\in\Fq^{k\times n}, w\in\Nstar, \mathbf{y}\in \Fq^{n}$, existe-t-il $\mathbf{e}$ de poids exactement $w$ tel que $\mathbf{y}-\mathbf{e} = \mathbf{mG}$ pour un certain $\mathbf{m}\in\Fq^{k}$.
\end{problem}


\begin{itemize}
	\item Pour la métrique de Hamming, ce problème est NP-complet \cite{BMT78, debris-alazardCryptographieFondeeCodes2019}. Ceci donne une information sur la complexité dans le \emph{pire cas}. Par ailleurs la complexité moyenne des meilleurs algorithmes connus, qu'ils soient classiques ou quantiques est exponentielle en le poids de l'erreur \cite{debris-alazardCryptographieFondeeCodes2019}.

	\item Pour la métrique rang, ce problème est prouvé NP-complet uniquement lorsque le code est matriciel général \cite{C01}. En revanche, dans le cadre des codes $\Fqm$-linéaires, on ne connaît qu'une réduction probabiliste polynomiale vers le problème \emph{SD} en métrique de Hamming \cite{GZ14}. Toutefois, on ne connaît pas à l'heure actuelle d'algorithmes de décodage génériques qui exploiteraient efficacement la $\Fqm$-linéarité, et les meilleurs algorithmes connus restent exponentiels \cite{debris-alazardCryptographieFondeeCodes2019}, même si des résultats récents donnent une meilleure compréhension des attaques algébriques du problème de décodage en métrique rang à l'aide de bases de Gröbner \cite{bardetAlgebraicAttackRank2020,bardetImprovementsAlgebraicAttacks2020}.
\end{itemize}

\vspace{\baselineskip}

En 1986, Niederreiter a proposé une variante duale du chiffrement de McEliece \cite{N86}. L'avantage de ce système réside dans la taille des chiffrés. En revanche, le temps de chiffrement et de déchiffrement est plus élevé.

D'autres problèmes ont été considérés pour construire des schémas de chiffrement à base de codes. En 2003, Augot et Finiasz ont ainsi proposé un cryptosystème dont la sécurité reposait entre autres sur le problème de reconstruction de polynômes \cite{AF03, AFL03}, problème lié au décodage de codes de Reed-Solomon en métrique de Hamming. Cependant, il a été aussitôt attaqué par Coron \cite{C04}. Le monde de la métrique rang possède un analogue remarquable des codes de Reed-Solomon : les codes de Gabidulin. Ils sont optimaux (tant en Hamming qu'en métrique rang) au sens où ils atteignent la borne de Singleton, et ils sont décodables efficacement jusqu'au rayon de décodage unique. En 2005, Faure et Loidreau ont alors introduit un analogue du problème de reconstruction de polynômes, adapté à la métrique rang, et ont proposé un nouveau schéma de chiffrement, analogue en métrique rang du système d'Augot et Finiasz \cite{FL05}. Ce système est très attractif au regard de la taille de ses clés, et de ses chiffrés, pour une vitesse de chiffrement et de déchiffrement très compétitive. Mais la joie est de courte durée, puisque le système a été attaqué en 2018 \cite{GOT18}. Une réparation a finalement été proposée par Renner, Puchinger et Wachter-Zeh en 2019 \cite{RPW19}, et c'est celle-ci qui nous intéressera par la suite.

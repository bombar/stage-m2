\subsection{Système original}
Dans \cite{FL05}, Cédric Faure et Pierre Loidreau ont introduit un nouveau système de chiffrement à clé publique, fondé entre autres sur le problème de reconstruction de $q$-polynômes, ou ce qui revient au même, sur le problème de décodage des codes de Gabidulin (au-delà du rayon de décodage unique). À la différence des systèmes \og à la McEliece \fg, ce schéma possède une clé publique de quelques kilo-octets seulement.


On considère $k, n, w, u$ des entiers naturels non nuls vérifiant $u < k < n$ et $n-k > w > \lfloor \frac{n-k}{2} \rfloor$. Le schéma de Faure-Loidreau utilise 3 corps finis différents : $\Fq, \Fqm$ et $\Fqmu$. Dans la suite, $Tr$ désigne l'opérateur Trace de l'extension $\Fqmu/\Fqm$ : $Tr(x) = x + x^{q^{m}} + \dots + x^{q^{m(u-1)}}$.


\vspace{\baselineskip}

\procedure[linenumbering=on, skipfirstln, mode=text]{Génération des clés}{
	\textbf{input:} Paramètres $q, m, n, k, u, w$ \pcskipln \\
	\textbf{output:} Clé secrète $(\mathbf{x, P})$ et clé publique $(\mathbf{g}, k, \mathbf{k_{pub}}, t_{pub})$ \\
	Choisir aléatoirement $\mathbf{g}\in \Fqm^{n}$ avec $\rk(\mathbf{g}) = n$. \\
	Choisir aléatoirement $\mathbf{x} \in \Fqm^k$ de sorte que $(x_{k-u+1}, \dots, x_k)$ forme une base de $\Fqmu/\Fqm$. \\
	Choisir aléatoirement $\mathbf{s} \in \Fqmu^w$ avec $\rk(\mathbf{s}) = w$. \\
	Choisir aléatoirement $\mathbf{P} \in GL_n(\Fq)$. \pcskipln \\
	\pclinecomment{On note $\mathbf{G}$ une matrice génératrice de $Gab_k(\mathbf{g})$} \\
	$\mathbf{z} \gets (\mathbf{s} \mid 0) \mathbf{P}^{-1}$ \\
	$\mathbf{k_{pub}} \gets \mathbf{xG + z}$ \\
	$t_{pub} \gets \left\lfloor \dfrac{n-k-w}{2} \right\rfloor$ \\
	$\pk \gets (\mathbf{g}, k, \mathbf{k_{pub}}, t_{pub})$ \\
	$\sk \gets (\mathbf{x, P})$ \\
	$\pcreturn (\pk, \sk)$
}

\vspace{\baselineskip}

\begin{pchstack}[center, space=1.5em]
	\procedure[linenumbering=on, skipfirstln, mode=text]{Algorithme de chiffrement}{
		\textbf{input:} Message clair $\mathbf{m} = (m_{1}, \dots, m_{k-u}, 0,\dots, 0) \in \Fqm^{k}$ \pcskipln \\
		\hspace{1.2cm} et clé publique $(\mathbf{g}, k, \mathbf{k_{pub}}, t_{pub})$ \pcskipln \\
		\textbf{output:} Message chiffré $\mathbf{c}$\\
		Choisir aléatoirement $\alpha \in \Fqmu\backslash \{0\}$. \\
		Choisir aléatoirement $\mathbf{e} \in \Fqm^n$ de sorte que $\rk(\mathbf{e}) = t_{pub}$. \pcskipln \\
		\pclinecomment{On note $\mathbf{G}$ une matrice génératrice de $Gab_k(\mathbf{g})$} \\
		$\mathbf{c} \gets \mathbf{mG} + Tr(\alpha\mathbf{k_{pub}}) + \mathbf{e}$\\
		$\pcreturn \mathbf{c}$
	}

	\procedure[linenumbering=on, skipfirstln, mode=text]{Algorithme de déchiffrement}{
		\textbf{input:} Message chiffré $\mathbf{c}$ et clé secrète $(\mathbf{x, P})$ \pcskipln \\
		\textbf{output:} Message clair $\mathbf{m}$\\
		$\mathbf{c'} \gets (\mathbf{cP})[w:]$ \\
		$\mathbf{g'} \gets (\mathbf{gP})[w:]$ \\
		$\mathbf{m'} \gets \text{ Décoder }\mathbf{c'} \text{ dans le code }Gab_k(\mathbf{g'})$. \\
		$\alpha \gets \sum_{i=k-u+1}^k m_i' x_i^\star$ \\
		$\mathbf{m} \gets \mathbf{m'} - Tr(\alpha \mathbf{x})$ \\
		$\pcreturn \mathbf{m}$
	}
\end{pchstack}

où
\begin{itemize}
	\item $\mathbf{y}[w:]$ est le vecteur $\mathbf{y}$ dans lequel on a retiré les $w$ premières composantes.
	\item $(x_{k-u-1}^{\star}, \dots, x_{k}^{\star})$ est la base duale de $(x_{k-u+1}, \dots, x_k)$ au sens où $Tr(x_{i}x_{j}^{\star}) = \delta_{i,j}$. Une telle base existe et est unique, et peut-être calculée en inversant un système linéaire (\cite{mcelieceFiniteFieldsComputer1987}).
\end{itemize}


Une analyse de complexité des différentes étapes, et une preuve de correction de l'algorithme de déchiffrement pourront être trouvées dans l'article original de Faure et Loidreau \cite{FL05}.


\subsection{Attaque et réparation}

Ce système est très attractif étant donné son efficacité et la taille de ses clés, cependant Gaborit, Otmani et Talé-Kalachi ont prouvé que dans la plupart des cas il était possible de retrouver une clé secrète possible à partir de $\mathbf{k_{pub}}$ en \bigO{n^3} opération dans le gros corps $\Fqmu$ \cite{GOT18} lorsque $w \le \lfloor\frac{u}{u+1}(n-k)\rfloor$, ce qui était toujours le cas avec les paramètres proposés par Faure et Loidreau.

Wachter-Zeh et al. ont démontré dans \cite{RPW19} que cette attaque était équivalente à décoder la clé publique dans un code de Gabidulin entrelacé. Ils ont alors proposé de modifier la génération de la clé privée de sorte que l'on soit toujours dans un cas d'échec de ce décodage par tous les algorithmes connus, en imposant une certaine condition sur la génération des clés, et donc une certaine structure supplémentaire.

Durant mon stage je me suis intéressé à la réparation de ce système et aux codes de Gabidulin entrelacés. J'ai proposé un nouvel algorithme de décodage des codes entrelacés, ce qui fournit d'une part une attaque alternative au système original de Faure-Loidreau. D'autre part, elle permet de retrouver et donner une autre intuition de la condition imposée par Renner, Puchinger et Wachter-Zeh dans leur article.

\title{Analyse de la sécurité d'un schéma de chiffrement à base de codes en métrique rang}

\author{Maxime \textsc{Bombar} encadré par Alain \textsc{Couvreur} \\ INRIA Saclay, équipe GRACE}

\date{09 Mars - 31 Juillet 2020}

\maketitle
% \pagestyle{empty} %
% \thispagestyle{empty}

%% Attention: pas plus d'un recto-verso!
% Ne conservez pas les questions


\subsection*{Le contexte général}

% De quoi s'agit-il ?
% D'où vient-il ?
% Quels sont les travaux déjà accomplis dans ce domaine dans le monde ?

Les codes en métrique rang sont des espaces de matrices munis de la métrique suivante : la distance entre deux matrices est le rang de leur différence. Cette métrique est utilisée pour corriger des pertes de paquets dans un réseau (\emph{Network Coding}) ou pour le codage \emph{espace-temps} servant à améliorer la fiabilité des systèmes de communication sans fils avec plusieurs émetteurs. Par ailleurs, la cryptographie à base de codes correcteurs d'erreurs est considérée comme résistante à l'ordinateur quantique, et la métrique rang permet d'obtenir des systèmes de chiffrement dont les tailles de clés et de messages chiffrés sont très attractifs. En particulier, dans la compétition du NIST pour la standardisation de la cryptographie post-quantique, deux propositions sélectionnées pour le 2ème tour sont à base de codes en métrique rang~\cite{AABBBBDGGGMMPSTZ19, AABBBDGZCH19}. Ces deux propositions n'ont pas été retenues pour le 3ème tour, dont la liste des candidats sélectionnés a été publiée le 22 Juillet 2020 ; le NIST n'ayant pas jugé cette famille assez mature. Cependant, dans son rapport, il encourage fortement la recherche en cryptographie à base de codes en métrique rang.

\subsection*{Le problème étudié}

% Quelle est la question que vous avez abordée ?
% Pourquoi est-elle importante, à quoi cela sert-il d'y répondre ?
% Est-ce un nouveau problème ?
% Si oui, pourquoi êtes-vous le premier chercheur de l'univers à l'avoir posée ?
% Si non, pourquoi pensiez-vous pouvoir apporter une contribution originale ?

Au cours de mon stage, je me suis intéressé dans un premier temps aux codes correcteurs en métrique rang, et plus précisément aux codes de Gabidulin~\cite{G85} qui sont à la métrique rang ce que les codes de Reed-Solomon~\cite{macwilliamsTheoryErrorCorrectingCodes1977} sont à la métrique de Hamming. Les codes de Gabidulin sont des codes algébriques, rapidement décodables lorsque le poids de l'erreur est inférieur à la moitié de la distance minimale, mais au delà le problème reste très largement ouvert. C'est précisément la difficulté de ce problème qui est utilisée dans le système de chiffrement de Faure-Loidreau~\cite{FL05}. Une attaque a été récemment proposée par Gaborit, Otmani et Talé-Kalachi~\cite{GOT18} et une réparation a été proposée par Renner, Puchinger et Wachter-Zeh pour y résister~\cite{RPW19}. L'objectif du stage était d'étudier les codes en métrique rang, les polynômes tordus, et leurs applications en cryptographie avant d'étudier plus précisément le système de Faure-Loidreau et sa réparation, voire de trouver une nouvelle attaque.

\subsection*{La contribution proposée}

% Qu'avez vous proposé comme solution à cette question ?
% Attention, pas de technique, seulement les grandes idées !
% Soignez particulièrement la description de la démarche \emph{scientifique}.

Sous la direction d'Alain Couvreur, l'étude des codes en métrique rang m'a amené à chercher un nouvel algorithme à la Welch-Berlekamp de décodage des codes de Gabidulin, analogue à droite de l'algorithme proposé par Loidreau dans~\cite{loidreauWelchBerlekampAlgorithmDecoding2005}, et son application au décodage des codes entrelacés (voir à la Section~\ref{section:BWD}). Cette étude a donné lieu à une implémentation en Sage~\cite{SageMathMathematicalSoftware}, en adaptant une base de code en cours d'étude pour être intégrée dans la bibliothèque standard. Je suis d'ailleurs en contact avec Xavier Caruso, dans l'objectif de continuer à participer au développement des codes correcteurs en métrique rang dans Sage, et pour y intégrer {\it in fine} mon code.


J'ai aussi proposé une implémentation du cryptosystème de Faure-Loidreau en Sage, ainsi qu'une attaque utilisant l'algorithme de décodage susmentionné.

\subsection*{Les arguments en faveur de sa validité}

% Qu'est-ce qui montre que cette solution est une bonne solution ?
% Des expériences, des corollaires ?
% Commentez la \emph{robustesse} de votre proposition :
% comment la validité de la solution dépend-elle des hypothèses de travail ?

Le nouvel algorithme de décodage des codes de Gabidulin et sa généralisation aux codes entrelacés ont été implémentés en Sage. De nombreux tests ont été effectués pour vérifier expérimentalement qu'ils fonctionnaient bien. Une preuve de correction est proposée à la Section~\ref{section:BWD}.

Cet algorithme de décodage \emph{à droite} a permis de proposer une attaque alternative à celle de Gaborit, Otmani et Talé-Kalachi. Si elle reste inefficace contre la réparation, elle donne une nouvelle interprétation de la condition d'échec introduite par Renner, Puchinger et Wachter-Zeh.

L'implémentation en Sage est suffisamment efficace pour pouvoir être testée avec des paramètres \og réels \fg{}. En particulier, avec les paramètres annoncés pour atteindre 256 bits de sécurité, mon implémentation permet de retrouver une clé secrète en un peu plus d'une minute sur mon ordinateur personnel.

\subsection*{Le bilan et les perspectives}

% Et après ? En quoi votre approche est-elle générale ?
% Qu'est-ce que votre contribution a apporté au domaine ?
% Que faudrait-il faire maintenant ?
% Quelle est la bonne \emph{prochaine} question ?

L'étude de la réparation du système de Faure-Loidreau a mis en évidence une forte structure algébrique qui laisse à penser l'existence d'une vulnérabilité à une future attaque. Si aucune nouvelle attaque n'a pour l'instant été trouvée, une piste que nous avons évoquée à la toute fin du stage semble prometteuse et mérite d'être exploitée. Beaucoup de pistes restent encore à explorer afin de mieux comprendre les vulnérabilités potentielles du système. Une autre approche pourrait être au contraire de prouver la sécurité du système en réalisant une vraie réduction de sécurité, mais elle semble peu probable à l'heure actuelle.

Dans un cadre plus théorique, de nombreux problèmes ouverts existent autour des codes de Gabidulin, en particulier le problème de leur décodage en liste, lié au problème de la reconstruction de $q$-polynômes de $q$-degré supérieur à la moitié de la distance minimale du code.

Je suis convaincu qu'une étude passant par des expérimentations à l'aide de logiciels tels que SageMath permettront de mieux comprendre et de mieux appréhender les objets mathématiques mis en jeu.


Ce stage se poursuivra par une thèse que je débuterai le 1er Septembre sous la direction d'Alain Couvreur.


\subsection*{Remerciements}

Tout d'abord je tiens à remercier Alain Couvreur de m'avoir fait découvrir le monde des codes correcteurs, tout d'abord via son cours au MPRI et puis évidemment durant ce stage. Je le remercie aussi d'avoir été disponible durant toute la période du confinement. Elle n'a pas été très facile, mais Alain a toujours été présent, quitte à faire des visioconférences tard le soir. Merci de m'avoir invité au groupe de travail codes-crypto à INRIA Paris dès nos premiers échanges autour du stage en fin d'année 2019. Enfin je le remercie pour la confiance qu'il m'accorde en acceptant de diriger ma thèse, dans la continuité de ce stage, et pour m'avoir aidé à préparer les dossiers de financement. Cette thèse sera financée par un contrat doctoral CDSN, et le sujet s'annonce passionnant.
Ensuite je voudrais remercier tous les membres de l'équipe Grace, pour leur accueil chaleureux. En particulier merci à l'équipe des doctorants et post-doctorants qui m'ont bien intégré alors même que ce stage a été en très grande partie fait à distance.
Enfin, je remercie mon colocataire Lev-Arcady, qui bien qu'il ait choisit de basculer vers le côté obscur des équations aux dérivées partielles, a accepté de relire une version finale de ce rapport et de m'indiquer encore des erreurs de typographie qui m'avaient échappé.

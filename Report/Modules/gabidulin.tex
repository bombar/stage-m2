Les codes de Gabidulin sont définis à partir de certains anneaux de polynômes dits \emph{polynômes linéaires}.

\subsection{Anneaux de polynômes linéaires}

Les polynômes linéaires sont un cas particulier de polynômes tordus (\emph{skew polynomials}) étudiés par {\O}re dans les années 1930 \cite{oreContributionsTheoryFinite1934, oreSpecialClassPolynomials1933}.

\begin{defi}
	On appelle \emph{polynôme linéaire}, ou encore $q$-polynôme, un polynôme de la forme

	\[
		P = p_0X + p_1X^q + \dots + p_tX^{q^t},\; p_i\in \Fqm,\; p_t \neq 0.
	\]
	L'entier $t$ est appelé $q$-degré de $P$ et est noté $deg_q(P)$.
\end{defi}

\emph{Notation :} Par analogie avec la syntaxe de SageMath, l'ensemble des $q$-polynômes de $\Fqm$ sera noté $\Fqm[X, \sigma]$ où $\sigma : x\mapsto x^{q}$.

\vspace{\baselineskip}

L'automorphisme de Frobenius étant linéaire, les $q$-polynômes induisent des applications linéaires sur $\Fqm$ vu comme $\Fq$-espace vectoriel. Si le produit classique de deux $q$-polynômes n'est en général pas linéaire, leur composition reste bien un $q$-polynôme. Ils forment alors un anneau \emph{non commutatif}. Par ailleurs, {\O}re a montré qu'ils possédaient une division euclidienne analogue à celle que l'on connaît dans le cadre des polynômes classiques.

\begin{defi}
	Soient $A,B$ deux polynômes linéaires. Alors
	~\begin{itemize}
		\item \textbf{division à droite} $A$ admet un quotient à droite $Q$ par $B$ s'il existe un $q$-polynôme $R$ tel que $A = Q\circ B + R$ et $\deg_q(R) < \deg_q(B)$.
		\item \textbf{division à gauche} $A$ admet un quotient à gauche $S$ par $B$ s'il existe un $q$-polynôme $T$ tel que $A = B\circ S + T$ et $\deg_q(T) < \deg_q(B)$.
	\end{itemize}
\end{defi}

\begin{prop}
	Muni des lois $+$ et $\circ$, $\Fqm[X, \sigma]$ est un  anneau non commutatif. De plus, il est euclidien à gauche et euclidien à droite.
\end{prop}

\subsection{Analogue en métrique rang des codes de Reed-Solomon}

Les codes de Reed-Solomon en métrique de Hamming introduits en 1960 (\cite{reedPolynomialCodesCertain1960a}) ont été largement étudiés et possèdent de nombreuses propriétés d'optimalité (codes MDS, algorithmes de décodage efficace, décodage en liste~...). En 1985, Ernst Gabidulin propose un analogue en métrique rang de ces codes correcteurs \cite{G85}.

\begin{defi}[Code de Gabidulin]
	Soit $\mathbf{g} := (g_{1}, \dots, g_{n})\in\Fqm^{n}$, de $\Fq$-rang $n$ (c'est-à-dire que les $g_{i}$ sont linéairement indépendants sur $\Fq$, ce qui n'est possible que si $n\le m$ ce que nous supposerons par la suite). On appelle code de Gabidulin de longueur $n$, de dimension $k$ et de \emph{support} $\mathbf{g}$ l'ensemble :
	$$Gab_{k}(\mathbf{g}) := \{(f(g_{1}), \dots, f(g_{n})) \mid f\in \Fqm[X, \sigma], \deg_{q}(f) < k \}.$$
\end{defi}

\begin{rem}
	Le code $Gab_{k}(\mathbf{g})$ a une matrice génératrice de la forme

	\[
		\begin{pmatrix}
			g_{1}         & \dots  & g_{n}         \\
			\vdots        & \ddots & \vdots        \\
			g_{1}^{[k-1]} & \dots  & g_{n}^{[k-1]}
		\end{pmatrix}
	\]
	où $x^{[i]}$ est une notation pour $x^{q^{i}}$.
\end{rem}

De la même manière que pour les codes de Reed-Solomon, les $[n,k,d]$-codes de Gabidulin sont d'une certaine manière des codes optimaux en métrique rang : ils sont dits MRD (\emph{Maximum Rank Distance}), \ie{} ils satisfont l'égalité dans l'inégalité de Singleton : $d = n-k+1$. Ils sont donc aussi optimaux pour la métrique de Hamming puisque $\mathbf{Rang}(x) \le w_{H}(x)$ pour $x$ un élément de $\Fqm^{n}$. On en déduit que le rayon de décodage unique de $Gab_{k}(\mathbf{g})$ est $\lfloor \frac{n-k}{2}\rfloor$.

Enfin, terminons cette section en mentionnant une propriété importante des codes de Gabidulin :

\begin{prop}
	Soit $Gab_{k}(\mathbf{g})$ un code de Gabidulin de longueur $n$ et de matrice génératrice $\mathbf{G}$. Soit $\mathbf{T}\in GL_{n}(\Fq)$. Alors $\mathbf{GT}$ est une matrice génératrice du code de Gabidulin $Gab_{k}(\mathbf{gT})$.
\end{prop}

\subsection{Décodage des codes de Gabidulin}
Par définition, les codes de Gabidulin sont très liés aux polynômes linéaires. Plusieurs algorithmes de décodage existent, certains tirent à profit les propriétés de ces $q$-polynômes. Dans cette section, certains énoncés sont légèrement différents de ceux existants dans la littérature citée. Par soucis de complétude, leurs preuves sont trouvables en annexe mais ne sont pas nécessaires à la lecture de ce rapport.

\subsubsection{Structure des racines et interpolation de \texorpdfstring{$q$}{q}-polynômes}

La nature linéaire du $q$-polynôme permet à l'ensemble de ses racines de former un espace vectoriel : il s'agit de son noyau en tant qu'application $\Fq$-linéaire. On a par ailleurs un lien entre la dimension de cet espace et le $q$-degré du polynôme. Des preuves des propriétés suivantes pourront être trouvées dans \cite{muratResultantsOrePolynomials2014}.

\begin{prop}\label{racines d'un q-polynome}
	Soit $P$ un $q$-polynôme de $q$-degré $k\neq 0$. Alors l'ensemble de ses racines $V$ forme un espace vectoriel de dimension au plus $k$ : $\dim\Ker P \le \deg_{q}P$.
	De plus, il est possible de trouver une base des racines de $P$ en $O(km)$ opérations dans $\Fqm$.
\end{prop}

On a même de façon plus surprenante le résultat suivant d'interpolation

\begin{thm}\label{interpolation}
	Soit $V$ un espace vectoriel de dimension $k$. Alors il existe un unique $q$-polynôme unitaire $P$ de $q$-degré $k$ tel que $V$ soit l'ensemble des racines de $P$. Si de plus on connaît une base de $V$, la construction de ce polynôme peut se faire en $O(k^2 + k\log(m))$ opérations dans $\Fqm$.
\end{thm}

Il nous permet d'écrire un analogue du célèbre théorème d'interpolation de Lagrange, pour les $q$-polynômes.

\begin{thm}[Interpolation de Lagrange linéaire]\label{lagrange}
	Soit $(g_1, \dots, g_n) \in \Fqm^n$ formé d'éléments linéairement indépendants. Soit $(y_1, \dots, y_n) \in \Fqm^n$. Il existe un unique $q$-polynôme $P$ de $q$-degré au plus $n-1$ tel que pour tout $i$, $P(g_i) = y_i$. Par ailleurs, on peut calculer $P$ en $O(n^3 + n^2\log(m))$ opérations dans $\Fqm$.
\end{thm}
Il est intéressant de noter que dans le cas classique des polynômes en une indéterminée, on considérait une famille d'éléments distincts. L'analogue dans le cas des polynômes linéaires est la liberté de la famille.
\begin{proof} Voir en annexe \ref{preuve lagrange}.
\end{proof}

Puchinger et Wachter-Zeh ont proposé des algorithmes plus rapides pour effectuer ces opérations. Une analyse plus fine de la complexité pourra être trouvée dans \cite{puchingerFastOperationsLinearized2017}.

\subsubsection{Reconstruction de \texorpdfstring{$q$}{q}-polynômes et décodage} \label{decodage à gauche}

Toujours dans l'analogie avec la métrique de Hamming, Loidreau introduit dans \cite{loidreauWelchBerlekampAlgorithmDecoding2005} un algorithme de décodage des codes de Gabidulin \og à la Berlekamp-Welch \fg, fondé sur le problème de reconstruction de polynômes linéaires.

\paragraph{Reconstruction de Polynômes Linéaires (RPL)}
\begin{description}
	\item[DONNÉES] $\mathbf{y}=(y_1,\dots,y_n)\in\Fqm^n$, $\mathbf{g}=(g_1, \dots, g_n)\in\Fqm^n$ de rang $n$, $k, t$ des entiers naturels.
	\item[PROBLÈME] Trouver l'ensemble des couples $(V, f)$ où $V$ est un $q$-polynôme non nul de $q$-degré $\le t$ et où $f$ est un $q$-polynôme de $q$-degré $<k$, tel que
	      \[
		      \, V(y_i) = V\circ f(g_i) \mbox{ pour }1\le i\le n.
	      \]
\end{description}

\paragraph{Décodage des codes de Gabidulin}
\begin{description}
	\item[DONNÉES] $\mathcal{G} = \Gab_k(\mathbf{g})\subset \Fqm^n$ un code de Gabidulin, $\mathbf{y}=(y_1,\dots,y_n)\in\Fqm^n$ et $t\in\Nstar$
	\item[PROBLÈME] Trouver l'ensemble des $\mathbf{c}\in\mathcal{G}$ tel qu'il existe $\mathbf{e}\in\Fqm^n$ de rang $\le t$ tel que $$\mathbf{y} = \mathbf{c} + \mathbf{e}.$$
\end{description}

Le problème de la reconstruction des polynômes linéaires est supposé difficile lorsque le nombre d'erreurs se situe au-delà de la capacité de correction du code de Gabidulin associé, ce qui justifie son utilisation en cryptographie. Il est lié au problème de décodage par la proposition suivante:

\newpage

\begin{prop}[Équivalence entre reconstruction de $q$-polynômes et décodage.]\label{equivalence rpl-decodage}
  ~\begin{itemize}
	\item Si $(V, f)$ est une solution du problème de reconstruction $\mathbf{RPL}(\mathbf{y}, \mathbf{g}, k, t)$, alors $c := f(\mathbf{g})$ est une solution du problème de décodage $\mathbf{Decodage}(\Gab_{k}(\mathbf{g}), \mathbf{y}, t)$.
	\item Réciproquement, si $\mathbf{c} = f(\mathbf{g})$ est une solution de $\mathbf{Decodage}(\Gab_{k}(\mathbf{g}), \mathbf{y}, t)$ alors on peut trouver en temps polynômial un $q$-polynôme $V$ de $q$-degré au plus $t$ tel que $(V, f)$ soit une solution de $\mathbf{RPL}(\mathbf{y}, \mathbf{g}, k, t)$.
  \end{itemize}
\end{prop}

\begin{proof} Voir en annexe \ref{preuve equivalence rpl-decodage}.
\end{proof}

De cette observation on peut déduire un algorithme de décodage des codes de Gabidulin, fondé sur ce principe de reconstruction de $q$-polynômes. En effet, décoder un mot bruité $\mathbf{y}$ revient à résoudre le système

\begin{align}
	\left\lbrace \begin{array}{l}
		V(y_i) = V\circ f (g_i), \quad i=1,\dots, n \\
		\deg_q(V) \le t                             \\
		\deg_q(f)<k                                 \\
	\end{array} \right. \label{WBQ}
\end{align}
dont les inconnues sont les coefficients de $V$ et de $f$. Le $q$-polynôme $V$ est en quelque sorte le polynôme \emph{localisateur} de l'erreur. C'est lui qui définit dans quel espace elle peut évoluer. C'est un système \emph{non linéaire} de $n$ équations à $k+t+1$ inconnues. Pour le résoudre, une méthode serait de rajouter les équations définissant le corps et de déterminer une base de Gröbner du système. Cependant, de façon analogue à la métrique de Hamming, il est possible d'utiliser une méthode de linéarisation et chercher à résoudre le système

\begin{align}
	\left\lbrace \begin{array}{l}
		V(y_i) = N (g_i), \quad i=1,\dots, n \\
		\deg_q(V) \le t                      \\
		\deg_q(N)\le k + t - 1.              \\
	\end{array} \right. \label{WBL}
\end{align}

Ce nouveau système est \emph{linéaire} et ses inconnues sont les coefficients de $N$ et de $V$. Il y a $n$ équations, et $k + 2t + 1$ inconnues. Il est plus général, au sens de la proposition suivante

\begin{prop}
	Toute solution $(V, f)$ de (\ref{WBQ}) donne une solution $(V, V\circ f)$ de (\ref{WBL}).
\end{prop}


Si l'espace des solutions de (\ref{WBL}) est de dimension $0$, on en déduit qu'il n'y a pas de mot de code à distance inférieure ou égale à $t$. Mais on a mieux :


\begin{thm}\label{decodage WBL}
	Si $t = \lfloor (n-k)/2 \rfloor$ et si $\mathbf{e}$ est de rang $\le t$ et vérifie $\mathbf{y} = f(\mathbf{g}) + \mathbf{e}$ alors pour toute solution $(V, N) \neq (0,0)$ du système \eqref{WBL} on a $N = V\circ f$.
\end{thm}

\begin{proof}voir annexe \ref{preuve decodage WBL}.
\end{proof}

Autrement dit, jusqu'au rayon de décodage unique, il suffit de considérer n'importe quelle solution $(V,N) \neq (0,0)$ de (\ref{WBQ}) et de faire la division euclidienne à gauche de $N$ par $V$ pour retrouver $f$.

\subsection{Code de Gabidulin entrelacé}

\subsubsection{Généralités}

Un code de Gabidulin entrelacé, d'ordre (ou de degré) d'entrelacement $u$, peut être vu comme $u$ codes de Gabidulin juxtaposés. Nous nous intéresserons dans ce rapport plus précisément aux codes de Gabidulin entrelacés \emph{verticalement}, introduits par Loidreau et Overbeck dans \cite{loidreauDecodingRankErrors2006}. Je vais suivre ici la présentation faite dans la thèse d'Antonia Wachter-Zeh \cite{wachter-zehDecodingBlockConvolutional2013}.

\begin{defi}[Code de Gabidulin entrelacé]
	Soit $\mathbf{g} = (g_{1} \dots, g_{n}) \in \Fqm^{n}$ une famille d'éléments linéarement indépendants sur $\Fq$. Le code de Gabidulin entrelacé, de longueur $n$, de dimensions $\mathbf{k} := [k^{(1)}, \dots, k^{(u)}]$ (avec $k^{(i)} \le n$), de support $\mathbf{g}$ et d'ordre d'entrelacement $u$ est le code défini par

	\[
		IGab(u, \mathbf{k})(\mathbf{g}) := \left\lbrace \begin{pmatrix}\mathbf{c^{(1)}} \\ \mathbf{c^{(2)}} \\ \vdots \\ \mathbf{c^{(u)}} \end{pmatrix} \mid \mathbf{c^{(i)}}\in Gab_{k^{(i)}}(\mathbf{g})\right\rbrace.
	\]

	Lorsque toutes les dimensions sont égales, le code est appelé code de Gabidulin \emph{homogène} et est noté tout simplement $IGab[u, k](\mathbf{g}).$
\end{defi}

\begin{rem}
	Lorsque $u=1$, ceci coïncide avec le code de Gabidulin usuel de dimension $k$ et de support $\mathbf{g}$.
\end{rem}

Dans toute la suite, on ne s'intéressera qu'aux codes homogènes. Ces codes sont intéressants puisqu'ils atteignent eux aussi la borne de Singleton :

\begin{thm}[Les codes de Gabidulin entrelacés homogènes sont MRD]
	Soit $IGab[u, k](\mathbf{g})$ un code de Gabidulin entrelacé. Alors c'est un code de longueur $n$, de dimension $k$ et de distance minimale $d = n-k+1$.
\end{thm}

Une preuve de ce théorème peut-être trouvé dans \cite{wachter-zehDecodingBlockConvolutional2013}.

\subsubsection{Modèle d'erreur considéré}\label{subsection:modele d'erreur}

Tout comme pour les codes de Gabidulin, il existe une version entrelacée des codes de Reed-Solomon \cite{bleichenbacherDecodingInterleavedReed2007}. En métrique de Hamming, une erreur de poids $t$ est un vecteur ayant exactement $t$ \emph{composantes} non nulles. Le \emph{support} d'une telle erreur est alors l'ensemble des indices de ces composantes non nulles. Dans le cadre des codes de Reed-Solomon entrelacés, on considère que les erreurs de poids $t$ sont des matrices $\mathbf{E}\in\Fqm^{u\times n}$ ayant exactement $t$ \emph{colonnes} non nulles. Autrement dit, les \emph{erreurs} sur chaque code de Reed-Solomon interviennent \emph{au même endroit}, ou du moins le support de chaque erreur est inclus dans un \emph{même} ensemble de cardinal $t$, appelé le \emph{support} de l'erreur entrelacée. On dit que les erreurs sont \emph{synchrones}, ou qu'elles arrivent \emph{par paquets}. Ce modèle est par exemple largement utilisé pour corriger des erreurs introduites par un dommage physique sur un support de stockage.

En métrique rang, on peut définir plusieurs notions de support (voir la définition \ref{support en metrique rang}). Dans \cite{loidreauDecodingRankErrors2006}, Loidreau considère un modèle d'erreur dans lequel l'erreur est une matrice $\mathbf{E} \in \Fqm^{{u\times n}}$ telle que le \emph{support colonne} de chaque ligne est inclus dans un même espace vectoriel $\mathcal{E}$ de dimension $t$ appelé le \emph{support} de l'erreur : Les erreurs sur chaque mot de Gabidulin ont donc un \emph{support colonne commun}. Dans ce modèle, un récepteur reçoit $u$ messages $\mathbf{y^{(1)}}, \dots, \mathbf{y^{(u)}}$ tels que

\[
	\left\lbrace
	\begin{array}{l}
		\mathbf{y^{(i)}} = \mathbf{c^{(i)}} + \mathbf{e^{(i)}},\quad 1\le i\le n \\
		\mathbf{e^{(i)}} \in \mathcal{E}.
	\end{array}\right.
\]


Ainsi, en appliquant l'algorithme de Welch-Berlekamp de la section précédente sur chacune des lignes, on peut corriger les erreurs jusqu'à $t = \lfloor \frac{n-k}{2}\rfloor$. Mais il est possible de faire mieux en tirant parti de la structure de l'erreur :

\vspace{\baselineskip}

Soit $V$ le $q$-polynôme de $q$-degré $t$ dont les racines sont exactement $\mathcal{E}$ (le support commun des erreurs). Celui-ci s'annule donc sur chacune des lignes et le problème de décodage est lié à la résolution d'un système de la forme

\[
	\left\lbrace \begin{array}{ll}
		V(\mathbf{y^{(i)}}) = (V\circ f_{i}) (\mathbf{g}) &              \\
		\deg_q(V) \le t                                   & i=1,\dots, u \\
		\deg_q(f_{i})< k.                                 &              \\
	\end{array} \right.
\]

\noindent Loidreau montre alors qu'en adaptant l'algorithme de Welch-Berlekamp présenté à la section précédente, il est possible de retrouver le mot du code de Gabidulin entrelacé avec grande probabilité tant que $t \le \lfloor \frac{u}{u+1}(n-k) \rfloor$ (et dépassant donc la capacité de décodage pour $u \ge 2$).

Dans la suite de ce rapport, nous nous intéresserons à un autre modèle, dans lequel une erreur de poids $t$ va être une matrice $\mathbf{E} \in \Fqm^{u\times n}$ de $\Fq$-rang $t$. C'est-à-dire que la matrice de $\Fq^{um\times n}$ obtenue en étendant chaque ligne de $\mathbf{E}$ dans une base de l'extension $\Fqm/\Fq$ est de rang $t$. Soit $\mathbf{Y} = \mathbf{C} + \mathbf{E}$ un mot bruité avec $\mathbf{C} \in IGab[u, k](\mathbf{g})$ et $\mathbf{E}\in \Fqm^{u\times n}$ une erreur de poids $t$. Comme précédemment, en regardant ligne par ligne, cette équation se décompose en $u$ équations de la forme $y_{i} = c_{i} + e_{i}$ où $c_{i}\in Gab_{k}(\mathbf{g})$ et $e_{i}\in \Fqm^{n}$ vérifie $\rk(e_{i}) \le t$. Décoder $\mathbf{Y}$ revient alors à décoder \emph{en parallèle} $u$ mots de Gabidulin bruités par une erreur de rang au plus $t$. Par la même remarque que ci-dessus, il est donc facile de décoder jusqu'au rayon de décodage unique $t = \lfloor \frac{n-k}{2} \rfloor$. Là encore, il est possible de faire mieux et de décoder jusqu'à $t = \lfloor \frac{u}{u+1}(n-k)\rfloor$. Différentes approches pour décoder de tels codes sont présentées dans \cite{wachter-zehDecodingBlockConvolutional2013}.
Dans le cadre de mon stage, j'ai proposé et implémenté un autre algorithme de décodage des codes de Gabidulin entrelacés dans ce modèle d'erreur, dans le cas où $n=m$.

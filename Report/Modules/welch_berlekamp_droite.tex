\label{section:BWD}
Commençons par le cas d'un simple code de Gabidulin (degré d'entrelacement $1$).

\subsection{Un analogue à droite du problème de décodage}

On se donne un mot $\mathbf{y} = (y_1, \dots, y_n)$ tel qu'il existe un mot de code $\mathbf{c} = (c_1, \dots, c_n)$ et $\mathbf{e} = (e_1, \dots, e_n)$ de rang $t$ tels que
\begin{equation}
	\mathbf{y} = \mathbf{c} + \mathbf{e} \label{mot bruité}
\end{equation}

D'après le Théorème~\ref{lagrange}, il existe trois $q$-polynômes $Y, C, E$ de $q$-degré strictement inférieur à $n$ tels que pour tout $i$, $$Y(g_i) = y_i, C(g_i) = c_i \text{ et } E(g_i) = e_i$$ avec $\deg_q(C) < k$ par définition du code de Gabidulin.

On va supposer dans la suite que $n=m$. Ainsi, $(g_1, \dots, g_n)$ forme une base de $\Fqn$ vu comme $\Fq$-espace vectoriel et l'équation \eqref{mot bruité} se traduit par l'égalité de $q$-polynômes

\[
	Y = C + E
\]
où $E$ est de rang $t$ (au sens de l'application linéaire induite). Par interpolation, le problème de décodage devient donc :

\paragraph{Décodage par interpolation}
\begin{description}
	\item[DONNÉES] $(Y, \mathbf{g}, k, t)$ où $Y$ est un $q$-polynôme de $q$-degré $<n$, $\mathbf{g} = (g_{1}, \dots, g_{n})\in\Fqn^n$ est de $\Fq$-rang $n$, et $k, t$ sont des entiers positifs non nuls.
	\item[PROBLÈME] Trouver l'ensemble des $q$-polynômes $C$ de $q$-degré $<k$ tels qu'il existe un $q$-polynôme $E$ de rang~$\le t$, tel que $Y = C + E$.
\end{description}

\vspace{0.1\baselineskip}

L'algorithme précédent consistait à chercher un annulateur à gauche : un $q$-polynôme $V$ tel que $V\circ E = 0$. L'objectif ici est d'adapter l'algorithme de décodage en cherchant un annulateur à droite. Pour cela, une façon naturelle de procéder est de chercher à transposer le résultat du Théorème~\ref{interpolation}, ce qui fait intervenir une certaine notion de dualité. On introduit alors une forme bilinéaire symétrique convenable: celle issue de la forme trace.


\begin{prop}\label{forme bilinéaire trace}
	La forme trace de $\Fqn/\Fq$ est l'application $x \mapsto x + x^{q} + \dots + x^{q^{n-1}}$. Elle induit une forme bilinéaire, symétrique non dégénérée
	\[
		\fun{\inner{\cdot , \cdot}}{\Fqn\times\Fqn}{\Fq}{(a,b)}{Tr(ab).}
	\]
\end{prop}
\begin{proof}
	Pour $x\in\Fqn$ on note $\fun{\varphi_x}{\Fqn}{\Fqn}{y}{xy.}\phantom{\hspace{-10cm}}$ \\
	Si $x\neq 0$, $\varphi_x$ est une bijection de $\Fqn$ dans lui-même. En particulier $\#\{xy\mid y\in\Fqn\} = q^n$. Or, $Tr$ est un polynôme de degré $q^{n-1}$, donc ne peut pas s'annuler sur tout $\Img \varphi_x$. En particulier, $\Fqn^\perp = \{0\}$.
\end{proof}

Ce résultat nous permet alors de définir l'adjoint d'un endomorphisme $f:\Fqn\rightarrow\Fqn$ comme l'unique endomorphisme $f^{\ast}$ vérifiant, pour tous $a,b\in\Fqn$,

\[
	\inner{a, f(b)} = \inner{f^{\ast}(a), b}.
\]

\noindent L'adjoint de $f$ est encore un endomorphisme de $\Fqn$, de même rang que $f$. De plus, l'opérateur d'adjonction est $\Fq$-linéaire et on a pour tous endomorphismes $f$ et $g$ la propriété d'anticommutativité $(f\circ g)^{\ast} = g^{\ast}\circ f^{\ast}$. Rappelons que les $q$-polynômes induisent un endomorphisme de $\Fqn$. Par suite, on peut définir leur adjoint. C'est encore un $q$-polynôme, que l'on peut calculer à l'aide du lemme suivant :

\vspace{-0.5\baselineskip}

\begin{lemma}. \\
	\vspace{-\baselineskip}
	\begin{enumerate}[label=(\roman*)]
		\item \label{X est autoadjoint} $\forall a\in \Fqn$, $(aX)^{\ast} = aX$.
		\item \label{adjoint est son inverse} $\forall 1\le i \le n-1, (X^{{q^{i}}})^{\ast} = X^{q^{n-i}} = (X^{q^{i}})^{{-1}}.$
	\end{enumerate}
\end{lemma}

\begin{proof}
	Soient $a, x, y\in\Fqn$. Alors $\inner{x, (aX)(y)} = \inner{x, ay} = Tr(xay) = Tr(axy) = \inner{ax, y} = \inner{(aX)(x), y}$, ce qui démontre \ref{X est autoadjoint}. Soit $1\le i\le n-1$, et $x, y\in \Fqn$. $\inner{x, y^{q^{i}}} = Tr(xy^{q^{i}})$. Or, la trace est invariante par application du Frobenius. Par conséquent, $Tr(xy^{q^{i}}) = Tr((xy^{q^{i}})^{q^{n-i}}) = Tr(x^{q^{n-i}}y^{q^{n}}) = Tr(x^{q^{n-i}}y) = \inner{x^{q^{n-i}}, y}$ ce qui prouve \ref{adjoint est son inverse}.
\end{proof}


\begin{prop}
	Soit $d\le n$ et soit $P:=\sum_{i=0}^d a_iX^{q^i}$ un $q$-polynôme de $q$-degré $d<n$. Alors son adjoint est le $q$-polynôme
	\[
		P^\ast := \sum_{i=0}^d a_i^{q^{n-i}}X^{q^{n-i}}.
	\]
\end{prop}

\begin{proof}
	\[
		P^{\ast} = \sum_{i=0}^{d}(a_{i}X^{q^{i}})^{\ast} = \sum_{i=0}^{d}(X^{q^{i}})^{\ast}(a_{i}X)^{\ast} = \sum_{i=0}^{d} X^{q^{n-i}}a_{i}X = \sum_{i=0}^{d}a_{i}^{q^{n-i}}X^{q^{n-i}}.
	\]
\end{proof}

Cette dualité permet de lier le noyau d'un $q$-polynôme à celui de son adjoint :
\begin{prop} Soit $P$ un $q$-polynôme. Alors $\Img(P)^{\perp} = \Ker(P^{\ast})$ et $\Ker(P)^{\perp} = \Img(P^{\ast})$ où l'orthogonal est pris au sens de la forme bilinéaire trace (Proposition \ref{forme bilinéaire trace}).
\end{prop}

On peut enfin énoncer, et prouver, le théorème suivant, résultat dual au Théorème~\ref{interpolation} :

\begin{thm}\label{interpolation droite}
	Soit $E$ un $q$-polynôme de rang $t$. Alors il existe un $q$-polynôme $V$ de $q$-degré au plus $t$ tel que $EV=0$.
\end{thm}

\begin{proof}
	On cherche un $q$-polynôme $V$ tel que $\Ker(E) \supset \Img(V)$, ie $\Img(E^\ast) \subset \Ker(V^\ast)$. Or, $E$ est de rang $t$ donc il en est de même pour $E^\ast$. En particulier $\dim\Img(E^{\ast}) = t$. Soit $Q$ le $q$-polynôme unitaire de $q$-degré $t$ dont les racines sont exactement $\Img(E^\ast)$. Il est de la forme  $\sum_{i=0}^t a_i X^{q^i}$. On pose $P = Q^\ast$. Alors

	\begin{align*}
		P & = \sum_{i=0}^t a_i^{q^{n-i}}X^{q^{n-i}}                                                     \\
		  & = \sum_{i=0}^t a_i^{q^{n-i}}X^{q^{t-i + n -t}}                                              \\
		  & = \left(\sum_{i=0}^t a_i^{q^{n-i}}X^{q^{t-i}}\right) \circ X^{q^{n-t}}                      \\
		  & = \underbrace{\left(\sum_{i=0}^t a_{t-i}^{q^{n-t+i}}X^{q^{i}}\right)}_{V} \circ X^{q^{n-t}} \\
	\end{align*}
	$\Img(E^\ast) = \Ker(Q)$ donc $\Ker(E) = \Img(Q^{\ast}) = \Img(P)$. On en déduit que $E\circ P = 0$ ou encore $E\circ V\circ X^{q^{n-t}}=0$, et $V$ est bien de $q$-degré inférieur ou égal à $t$. On conclut alors en remarquant que $X^{q^{n-t}}$ est inversible, et donc $E\circ V = 0$.
\end{proof}

Revenons alors au problème de décodage : on dispose à présent de trois $q$-polynômes $Y, C, E$ de $q$-degré strictement inférieur à $n$ qui interpolent respectivement le mot reçu, le mot de code et l'erreur, avec $\deg_q(C) < k$, $\rk(E) = t$, et

\begin{align}
	Y = C + E. \label{interpolation code}
\end{align}


D'après le Théorème~\ref{interpolation droite}, on dispose d'un $q$-polynôme $V$ de $q$-degré au plus $t$ tel que $E\circ V = 0$. On a donc la relation suivante :

\[
	0 = E\circ V = Y\circ V - C\circ V
\]

\noindent ou encore

\begin{align}
	\left\lbrace
	\begin{array}{l}
		Y\circ V = C\circ V \\
		\deg_{q}V \le t     \\
		\deg_{q}C \le k-1.
	\end{array} \right.
	\label{BWD:Syst1}
\end{align}

\noindent Posons comme précédemment $N = C\circ V$ pour obtenir

\begin{align}
	\left\lbrace
	\begin{array}{l}
		Y\circ V = N    \\
		\deg_{q}V \le t \\
		\deg_{q}N \le k + t - 1.
	\end{array} \right.
	\label{BWD:Syst2}
\end{align}


\noindent Les liens entre les équations précédentes sont précisés par les deux propositions suivantes :

\begin{prop}
	Toute solution $(V,C)$ de \eqref{BWD:Syst1} donne une solution $(V,C\circ V)$ de \eqref{BWD:Syst2}.
\end{prop}

\noindent et

\begin{thm}
	On suppose que $E$ est de rang $t\le \lfloor \frac{n-k}{2} \rfloor$. Si $(V,N)$ est une solution différente de $(0,0)$ de \eqref{BWD:Syst2}, alors $N=C\circ V$ où $C = Y - E$ interpole le mot de code.
\end{thm}

\begin{proof} Soit $(V, N)$ une solution de \eqref{BWD:Syst2} différente de $(0,0)$, et soit $C$ de $q$-degré $<k$ interpolant le mot de code. Posons $R := N - C\circ V$. C'est donc un $q$-polynôme, de $q$-degré au plus $k+t-1$. Raisonnons par l'absurde, et supposons que $R\neq 0$. Alors

	\[
		(Y-C) \circ V = Y\circ V - C\circ V = N - C\circ V = R
	\]
	ou encore

	\begin{align}
		E\circ V = R.
	\end{align}

	On en déduit que $R$ est de rang au plus $t$. Or, par le théorème du rang adapté aux $q$-polynômes (en utilisant la Proposition \ref{racines d'un q-polynome}), on a

	\[
		n \le \deg_q(R) + \rk(R) \le k+t-1 + t = k+2t -1 \le n-1 < n
	\]

	\noindent ce qui est absurde. Par conséquent, $N-C\circ V = 0$ ou encore $N = C\circ V$.
\end{proof}

Dès lors, si $t\le \lfloor \frac{n-k}{2} \rfloor$, toute solution $(V,N) \neq (0,0)$ de \eqref{BWD:Syst2} permet de retrouver $C$ par division euclidienne à droite de $N$ par $V$. Encore faut-il savoir résoudre cette équation. Puisque $(g_{1}, \dots, g_{n})$ forme une base, elle est équivalente au système de $n$ équations

\begin{align}
	(Y \circ V)(g_{i}) = N(g_{i}) \mbox{ pour } i=1,\dots, n.
\end{align}


Malheureusement, ce système n'est pas $\Fqm$-linéaire (il est semi-linéaire) et il faut ruser un peu. L'idée est de passer à l'adjoint. En notant $y_{i}^{\ast} := Y^{\ast}(g_{i})$ on en déduit alors le système suivant :

\begin{align}
	V^{\ast}(y_{i}^{\ast}) = N^{\ast}(g_{i}) \mbox{ pour } i=1,\dots, n. \label{BWD:Systeme lineaire}
\end{align}

\noindent qui est bien linéaire et dont les inconnues sont les coefficients de $V^{\ast}$ et $N^{\ast}$ ou, de façon équivalente, les coefficients de $V$ et $N$. Il y a donc $k+2t+1$ inconnues, pour $n$ équations. Pour pouvoir exploiter l'espace des solutions de ce système, il est nécessaire que :

\[
\# inconnues \le \# equations + 1
\]

Ce qui est le cas lorsque $t \le \lfloor\frac{n-k}{2}\rfloor$. On obtient alors l'algorithme de décodage suivant :

\vspace{\baselineskip}

\procedure[linenumbering=on, skipfirstln, mode=text]{Algorithme de décodage à droite}{
	\textbf{input:}  $(\mathbf{y}, \mathbf{g}, k, t)$ où $\mathbf{g} = (g_{1}, \dots, g_{n})\in\Fqn^n$ est de $\Fq$-rang $n$ \pcskipln \\
	\hspace{1.2cm} avec $k, t$ des entiers positifs non nuls tel que $t \le \lfloor \frac{n-k}{2} \rfloor$, \pcskipln \\
	\hspace{1.2cm} et $\mathbf{y} = (y_{1}, \dots, y_{n})$ est un mot bruité par une erreur de poids $t$. \pcskipln \\
	\textbf{output:} $\mathbf{c} \in \Gab_{k}(\mathbf{g})$ tel que $\rk(\mathbf{y} - \mathbf{c}) = t$.\\
	Trouver $Y$ le $q$-polynôme de $q$-degré strictement inférieur à $n$ et interpolant $\mathbf{y}$ en $\mathbf{g}$.\\
	Calculer $Y^{\ast}$ l'adjoint de $Y$ et l'évaluer en $\mathbf{g}$ pour obtenir $\mathbf{y}^{\ast} = (Y^{\star}(g_{1}), \dots, Y^{\star}(g_{n}))$.\\
	Trouver une solution non nulle $(V_{0}, N_{0})$ du système $V_{0}^{\ast}(y_{i}^{\ast}) = N_{0}^{\ast}(g_{i})$. \\
	Calculer les adjoints $V$ et  $N$ de $V_{0}$ et $N_{0}$ respectivement. \\
	Trouver $C$ en calculant la division euclidienne à droite de $N$ par $V$. \\
	Retrouver le mot de code $\mathbf{c}$ en évaluant $C$ en $\mathbf{g}$.
}

\subsection{Implémentation en Sage}


Le stage m'a donné l'occasion de découvrir SageMath et de proposer une implémentation de cet algorithme de décodage. Pour cela, j'ai repris et adapté une base d'implémentation des codes de Gabidulin en Sage, et plus généralement des codes en métrique rang, avant d'implémenter l'algorithme de décodage \og à la Welch-Berlekamp \fg{} classique ainsi que la version à droite de la section précédente. Alain m'a mis en contact avec Xavier Caruso qui s'est montré intéressé par l'intégration de mon code dans la bibliothèque standard de Sage. Plusieurs tickets sont en cours de relecture afin de rendre ceci possible. Mon implémentation est accessible via ce lien Gitlab : \url{https://gitlab.inria.fr/mbombar/misc_sage}.

\subsection{Application au décodage des codes de Gabidulin entrelacés}

Considérons $G = IGab(u, k)(\mathbf{g})$ un code de Gabidulin entrelacé avec le modèle d'erreur présenté en Section~\ref{subsection:modele d'erreur}, et soit $\mathbf{Y} = \mathbf{C} + \mathbf{E}$ un mot bruité par une erreur $\mathbf{E} \in \Fqn^{u\times n}$ de poids $t$. En décomposant cette équation ligne par ligne, on obtient $u$ nouvelles équations de la forme $y_{i} = c_{i} + e_{i}$ où chaque $c_{i}$ est un mot de $Gab_{k}(\mathbf{g})$.

Pour tout $i$, notons $F_{i}$ le support ligne de $e_{i}$ et soit $F := F_{1} + \dots + F_{u}$ le support ligne de $\mathbf{E}$. Par hypothèse, $F$ est un sous-espace vectoriel de $\Fq^{n}$ de dimension $t$. Il existe un $q$-polynôme $P$ de rang $t$ tel que $F = \{xM_{P} \mid x\in\Fq^n \}$ où $M_{P}\in\Fq^{n\times n}$ est la matrice de (l'endomorphisme induit par) $P$ dans la base $\mathbf{g}$. D'après le Théorème~\ref{interpolation droite}, on dispose d'un annulateur à droite de $P$, c'est-à-dire un $q$-polynôme $V$ de $q$-degré $t$ tel que $PV = 0$, ce qui se traduit matriciellement par l'égalité $M_{P}M_{V} = 0$ où $M_{V}$ désigne la matrice de $V$ dans la base $\mathbf{g}$.

Par conséquent, on a

\[
	yM_{V} = 0 \mbox{ pour tout $y\in F$},
\]

\noindent d'où on déduit

\[
	\ext_{\mathbf{g}}(\mathbf{E})M_{V} = 0.
\]


Or, en considérant les $u$ blocs de $n$ lignes, cette dernière équation devient

\[
	M_{E_{i}}M_{V} = 0 \mbox{ pour $1\le i\le u$}
\]

\noindent où $E_{i}$ est l'interpolateur de $e_{i}$ de support $\mathbf{g}$. Autrement dit, $V$ est un annulateur à droite de $q$-degré $t$, commun à tous les $e_{i}$. Or, par interpolation, le problème de décodage donne un système de $q$-polynômes $Y_{i} = C_{i} + E_{i}$ qui se réécrit en $Y_{i}\circ V = C_{i}\circ V.$ Par la même technique que dans la section précédente, en posant $N_{i} = C_{i}\circ V$, on obtient le système linéaire

\begin{align}
	\left\lbrace
	\begin{array}{l}
		V^{\ast}(y_{j}^{\ast}) = N_{i}^{\ast}(g_{j}) \mbox{ pour } 1\le i \le u \mbox{ et } 1\le j \le n \label{BWD:Systeme entrelacé} \\
		\deg_{q}V \le t                                                                                                                \\
		\deg_{q}N_{i} \le k + t - 1.
	\end{array} \right.
\end{align}

\noindent C'est un système à $u\times n$ équations et $t+1 + u(k+t)$ inconnues. On peut alors espérer corriger l'erreur lorsque $t \le \lfloor \frac{u}{u+1}(n-k)\rfloor$.

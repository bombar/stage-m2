Rappelons que dans le système de chiffrement de Faure-Loidreau, la clé publique $\mathbf{k_{pub}}$ est de la forme $\mathbf{xG}+\mathbf{z}\in\Fqmu^{n}$ où $\mathbf{x}\in\Fqmu^{k}$, $\mathbf{G}$ est une matrice génératrice d'un certain code de Gabidulin de longueur $n$ et de dimension $k$ sur $\Fqm$ et $\mathbf{z}\in \Fqmu^{n}$ est une erreur de poids $w > \frac{n-k}{2}$ de sorte que le décodage direct de la clé soit considéré comme calculatoirement infaisable.

\subsection{Clé publique comme mot d'un code de Gabidulin entrelacé, bruité}

Dans \cite{RPW19}, Wachter-Zeh et al. ont introduit une nouvelle interprétation de la clé publique $\mathbf{k_{pub}}$ comme un mot d'un code de Gabidulin entrelacé homogène sur $\Fqm$, bruité par une erreur de poids $w$ : soit $\gamma = (\gamma_{1}, \dots, \gamma_{u}) \in \Fqmu^{u}$ une base de $\Fqmu/\Fqm$. Notons encore $Tr$ la trace de cette extension. En utilisant la $\Fqm$-linéarité, on a la relation suivante pour $i = 1, \dots, u$ :

\[
	Tr(\gamma_{i}\mathbf{k_{pub}}) = Tr(\gamma_{i}\mathbf{x})\mathbf{G} + Tr(\gamma_{i}\mathbf{z})
\]


Posons alors $\mathbf{K} := \begin{pmatrix} Tr(\gamma_{1}\mathbf{k_{pub}}) \\ \vdots \\ Tr(\gamma_{u}\mathbf{k_{pub}}) \end{pmatrix}$, $\mathbf{C} := \begin{pmatrix} Tr(\gamma_{1}\mathbf{x})\mathbf{G} \\ \vdots \\ Tr(\gamma_{u}\mathbf{x})\mathbf{G} \end{pmatrix}$ et $\mathbf{Z} := \begin{pmatrix} Tr(\gamma_{1}\mathbf{z}) \\ \vdots \\ Tr(\gamma_{u}\mathbf{z}) \end{pmatrix}$ de sorte que $\mathbf{K} = \mathbf{C} + \mathbf{Z}$.


Par ailleurs,

\[
	\mathbf{z} = \sum_{i=1}^{u} Tr(\gamma_{i}\mathbf{z})\gamma_{i}^{\ast}
\]

où $\gamma^{\ast}$ désigne la base duale de $\gamma$. En particulier $\rk(\mathbf{Z}) = \rk(\mathbf{z}) = w.$



$\mathbf{K_{pub}}$ est alors un mot bruité de $IGab(u, k)(\mathbf{g})$ où $\mathbf{g}$ est le support du code de Gabidulin défini par $\mathbf{G}$. Par conséquent, lorsque $w \le \lfloor\frac{u}{u+1}(n-k)\rfloor$, il est possible de retrouver $\mathbf{x}$ et $\mathbf{z}$ avec une grande probabilité en appliquant un algorithme de décodage de Gabidulin entrelacé. Tous les paramètres proposés par les auteurs du système sont dans ce cadre.

Wachter-Zeh et al. ont prouvé dans \cite{RPW19} que cette attaque était équivalente à l'attaque introduite par Gaborit, Otmani et Talé-Kalachi dans \cite{GOT18}, au sens où elles échouaient exactement dans les même cas. Leur remarque est la suivante : en notant $\zeta := \rk_{\Fqm}\mathbf{z}$ le rang de $\mathbf{z}$ sur $\Fqm$, tous les algorithmes connus jusqu'à présent pour décoder des codes de Gabidulin entrelacés échouent lorsque $\zeta < \frac{w}{n-k-w}$. C'est le cas en particulier lorsque $\zeta = 1$, ce qu'ils considèrent dans la suite de leur article.

L'idée à la fin du stage était d'appliquer le décodage à droite sur le cryptosystème, et de voir s'il était possible de faire mieux. Malheureusement, comme on va le voir dans la suite, il se trouve confronté aux mêmes difficultés que les autres algorithmes de décodage des codes entrelacés (par syndrome par exemple). Il fournit cependant une attaque alternative du système original de Faure et Loidreau.

\subsection{Application du décodage à droite}

J'ai implémenté en Sage une attaque sur ce système en exploitant le décodage à droite présenté dans ce rapport. Le code est disponible via \url{https://gitlab.inria.fr/mbombar/stage-m2/-/tree/master/Gabidulin/Faure-Loidreau}. Pour cela, j'ai réimplémenté le cryptosystème de Faure-Loidreau, en laissant certains paramètres libres pour pouvoir gérer facilement les tests.

L'attaque fonctionne de la façon suivante :	de la même manière que Wachter-Zeh et al. je vois la clé publique comme un mot de code de Gabidulin entrelacé. Afin d'avoir un décodage à droite, j'impose en plus que $n=m$, ce qui est vérifié dans tous les paramètres proposés, que ce soit pour le système original comme pour la réparation. Ensuite, je tente de décoder en cherchant un annulateur à droite de degré maximal ($\lfloor\frac{u}{u+1}(n-k)\rfloor$). Dans certains cas la résolution du système ne permet pas de retrouver cet annulateur par division euclidienne. Je décrémente alors le degré du polynôme recherché, ce qui fait diminuer le nombre d'inconnues du système linéaire sous-jacent, jusqu'à pouvoir décoder, ou passer sous la moitié de la distance minimale, auquel cas l'attaque échoue.

Expérimentalement on observe que l'attaque échoue dans les mêmes cas que les autres algorithmes connus, mais ce fait est encore à prouver par manque de temps durant le stage. Une analyse plus fine des cas d'échec serait aussi bienvenue et pourrait peut-être aider à mieux comprendre le comportement de l'attaque.


\subsection{Analyse}

Dans cette section, je donne une analyse de la situation, et une autre interprétation de la condition $\zeta < \frac{w}{n-k-w}$ proposée par Wachter-Zeh et al.

\subsubsection{Cas \texorpdfstring{$\zeta = 1$}{zêta = 1}}

Wachter-Zeh, Renner et Puchinger ont étudié diverses attaques contre la réparation de ce système lorsque $\zeta = 1$. En effet, c'est la gamme de paramètres qui selon eux apporte le meilleur niveau de sécurité puisque les algorithmes de décodage de codes de Gabidulin entrelacés appliqués dans ce cas amènent à faire une recherche exhaustive dans un espace d'autant plus gros que $\zeta$ est proche de $1$.

Je propose ici une explication alternative en faisant le lien avec le problème de décodage des codes de Gabidulin au delà de la moitié de la distance minimale qui est considéré comme un problème difficile. Il existe même des cas où ce décodage est impossible en temps polynomial (\cite{wachter-zehBoundsListDecoding2013}).

\begin{prop}
	Soit $\adv$ un adversaire capable de décoder de façon univoque et avec une probabilité non négligeable un code de Gabidulin entrelacé homogène, bruité par une erreur de poids au plus $\lfloor \frac{u}{u+1}(n-k) \rfloor$, et de rang $\zeta := 1$ sur $\Fqm$. Alors nous pouvons construire un adversaire $\bdv$ capable de décoder efficacement un code de Gabidulin au-delà de la moitié de la distance minimale.
\end{prop}

\begin{proof}
	On construit $\bdv$ de la façon suivante.

	Soit $\mathbf{G} := Gab_{k}(\mathbf{g})$ un code de Gabidulin de longueur $n$ et de dimension $k$. Soit $\mathbf{c} \in \mathbf{G}$ et soit $\mathbf{e}$ une erreur de rang $t > \frac{n-k}{2}$. Alors sur l'entrée $\mathbf{y} = \mathbf{c} + \mathbf{e}$, l'adversaire $\bdv$ choisit $u$ de sorte que $t < \frac{u}{u+1}(n-k)$
	et pose
	\[
		\mathbf{Y} := \begin{pmatrix}\mathbf{y} \\ \vdots \\ \mathbf{y} \end{pmatrix} = \begin{pmatrix}\mathbf{c} + \mathbf{e} \\ \vdots \\ \mathbf{c} + \mathbf{e}\end{pmatrix} = \mathbf{C} + \mathbf{E}
	\]

	\noindent où $\mathbf{C} \in IGab[u, k](\mathbf{g})$ et $\mathbf{E} = \begin{pmatrix}e \\ \vdots \\ e \end{pmatrix}$ est de rang $1$ sur $\Fqm$ et de poids $t$. Il lui suffit donc de lancer $\adv$ sur $\mathbf{Y}$ pour récupérer $\mathbf{C'} \in IGab[u, k](\mathbf{g})$ tel que $\mathbf{Y} - \mathbf{C'}$ soit de poids au plus $t$. Alors $\bdv$ vérifie que toutes les lignes de $\mathbf{C'}$ sont égales, et renvoie $\mathbf{c}$. Dans le cas contraire, $\bdv$ échoue.

	Sous l'hypothèse que $\adv$ sait décoder efficacement de façon univoque $\mathbf{C}$, $\bdv$ est donc capable de décoder $G$ au delà de $\frac{n-k}{2}$.
\end{proof}

\begin{rem}
	Si on restreint l'hypothèse sur $\adv$ en ne l'autorisant à décoder que si toutes les lignes sont distinctes, il suffit de modifier $\bdv$ pour lui faire générer $u-1$ mots de code $\mathbf{c_{1}}, \dots, \mathbf{c_{u-1}}$ dans $\mathbf{G}$ et de poser
	$\mathbf{Y} := \begin{pmatrix}\mathbf{y} \\ \mathbf{y} + \mathbf{c_{1}} \\ \vdots \\ \mathbf{y} +\mathbf{c_{u-1}}\end{pmatrix}$ pour conclure de façon analogue.
\end{rem}


\begin{rem}
  Si on sait décoder efficacement au delà de la moitié de la distance minimale, une attaque plus directe est de simplement appliquer ce décodage à la clé publique $\mathbf{k_{pub}}$ pour retrouver $\mathbf{x}$ et $\mathbf{z}$.
\end{rem}

\subsubsection{Cas \texorpdfstring{$\zeta > 1$}{zêta > 1}}


On peut généraliser le point de vue précédent : en effet, $\zeta$ peut se concevoir comme le degré d'entrelacement réel du code de Gabidulin. En appliquant un algorithme de décodage des codes de Gabidulin entrelacés tel celui présenté en Section~\ref{section:BWD}, on est amenés à résoudre un système linéaire à $n$ équations et $w+1+\zeta(w+1)$ inconnues. Donc si $w > \frac{\zeta}{\zeta+1}(n-k)$, le système est sous-contraint et l'espace des solutions est trop gros pour qu'il soit exploitable. Cette condition est équivalente à la condition proposée dans \cite{RPW19} et obtenue en considérant l'algorithme de décodage à base de syndromes.

En revanche, si $w \le \frac{\zeta}{\zeta+1}(n-k)$, on observe en pratique de bons résultats. Beaucoup de choses sont encore à comprendre sur le comportement de ce système, en particulier sur le rang du système en fonction du degré de l'annulateur recherché, et comprendre les cas d'échecs et de réussite, jusqu'où il faut faire diminuer le degré de l'annulateur pour arriver à décoder.

Lorsqu'elle fonctionne, cette attaque est assez rapide. Par exemple, pour un niveau annoncé de 256 bits de sécurité, je parviens à retrouver la clé secrète en un peu plus d'une minute avec les paramètres de \cite{RPW19} : $m = n = 83, k = 48, w = 21, u=4$ ; lorsque $\zeta = 2$ (le cas envisagé étant $\zeta = 1$).

\subsection{Un premier bilan}

Au vu de ce qui a été décrit dans la section ci-dessus, il peut sembler difficile de retrouver une clé secrète potentielle en décodant la clé publique dans un code entrelacé. On peut donc chercher des méthodes alternatives pour attaquer le système. En particulier, la réparation proposée dans \cite{RPW19} possède une très forte structure algébrique : les lignes de l'erreur sont liées deux à deux sur $\Fqm$. D'autres pistes sont à explorer pour chercher des vulnérabilités dans ce système de chiffrement. Nous avons d'ailleurs commencé à en explorer une à la fin du stage, qui sera probablement continuée durant ma thèse. À l'inverse, on pourra aussi chercher une vraie réduction de sécurité de ce système, par exemple au décodage en liste des codes de Gabidulin dont on sait qu'il existe des instances qu'il est impossible de décoder efficacement au-delà de la moitié de la distance minimale.

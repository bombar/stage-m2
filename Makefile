#!/usr/bin/make -f
#
# Requires latexmk.
#
# Maxime Bombar <bombar@crans.org>

## This Makefile is used to build pdf from a latex project. Use ```make help``` to display a summary of the rules.
##
## make MyFancyProject.pdf	: Builds MyFancyProject.pdf (and auxiliary files) from MyFancyProject.tex and fails if it doesn't exist.
## make MyFancyProject.warn	: Displays how many warnings and bad boxes there are when compiling MyFancyProject.
## make MyFancyProject.showwarn	: Displays the warnings and bad boxes for MyFancyProject.

.PHONY: all help clean cleanall
SHELL = /bin/bash


# latexmk is a swiss army knife of latex compilation. Option -pdf to output the pdf.
PDFLATEX := latexmk -pdf
CMD = cat $*.log | grep -iE
REGEXBOX := full..hbox
WARNINGS := warning


TEXDOCS = $(wildcard *.tex)
PDF := $(patsubst %.tex,%.pdf, $(TEXDOCS))
OUT := $(patsubst %.tex,%.out, $(TEXDOCS))
LOG := $(patsubst %.tex,%.log, $(TEXDOCS))


# Prevents some intermediate files to be automatically deleted...
.PRECIOUS: %.log %.pdf Makefile

%.log: %.tex
	$(PDFLATEX) $<

%.warn: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\)}"

%.showwarn: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\) for $*:}"
	@echo "${shell echo '$$( $(CMD) $(WARNINGS) | tail +2)' }"
	@echo "${shell echo $(\n)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\) for $*:}"
	@echo "${shell echo '$$( $(CMD) $(REGEXBOX))' }"

%.pdf: %.log
	@echo "${shell echo 'There are $$(( $$($(CMD) $(WARNINGS) | wc -l)-1 ))' warning\(s\)}"
	@echo "${shell echo 'There are $$($(CMD) $(REGEXBOX) | wc -l)' bad boxe\(s\)}"

## make all			: Builds every file in the current directory.
all: $(TEXDOCS)
	$(PDFLATEX) $^

## make rebuild			: Cleans and rebuilds every file.
rebuild: cleanall all

## make clean			: Removes every auto-generated file except for pdf.
clean:
	@echo -n "aux bbl blg dvi fdb_latexmk fls nav snm tdo toc thm vrb"|xargs -t -d ' ' -n 1 -I {} find . -iname "*.{}" -delete
	rm -f $(OUT)
	rm -f $(LOG)
	find . -iname "*flymake*" -delete
	find . -iname "*~" -delete
	find . -iname "\#*" -delete
	find . -type d  -iname 'auto' -exec rm -rf {} +
	find . -type d  -iname '.auctex-auto' -exec rm -rf {} +

## make cleanall			: Removes every auto-generated file.
cleanall: clean
	rm -f $(PDF)

## make help			: Displays this help.
help: Makefile
	@sed -n 's/^##//p' $<

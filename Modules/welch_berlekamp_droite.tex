\subsection{Un analogue à droite du problème de décodage}

Reprenons le problème de décodage. On se donne un mot $\mathbf{y} = (y_1, \dots, y_n)$ tel qu'il existe un mot de code $\mathbf{c} = (c_1, \dots, c_n)$ et $\mathbf{e} = (e_1, \dots, e_n)$ de rang $\le t$ tels que $$\mathbf{y} = \mathbf{c} + \mathbf{e}$$

D'après le théorème \ref{lagrange}, il existe trois $q$-polynômes $Y, C, E$ tels que pour tout $i$, $$Y(g_i) = y_i, C(g_i) = c_i \text{ et } E(g_i) = e_i$$

avec $\deg_q(C) < k$ par définition du code de Gabidulin. % Quitte à restreindre $E$ à l'espace engendré par $(g_1, \dots, g_n)$, on peut considérer que $rg(E) = rg(\mathbf{e}) \le t$.

On va supposer que $n=m$. Ainsi, $(g_1, \dots, g_n)$ forme une base de $\Fqm$ vu comme $\Fq$-espace vectoriel.

Par interpolation, le problème de décodage devient

\paragraph{Décodage par interpolation}
\begin{description}
\item[DONNÉES] $(Y, \mathbf{g}, k, t)$ où $Y$ est Un $q$-polynôme $Y$ de $q$-degré $<m$ et $\mathbf{g}\in\Fqm^n$ est de rang $n$.
\item[PROBLÈME] Trouver l'ensemble des $q$-polynômes $C$ de $q$-degré $<k$ tels qu'il existe un $q$-polynôme $E$ de rang $\le t$, tel que $Y = C + E$
\end{description}

\vspace{2\baselineskip}

L'algorithme précédent consistait à chercher un annulateur à gauche : Un $q$-polynôme $V$ tel que $V\circ E = 0$. L'objectif ici est d'adapter l'algorithme de décodage en cherchant un anulateur à droite.

\begin{defi}
  Pour tout $q$-polynôme $P$, on note $\gamma_i(P)$ le coefficient devant $X^{q^i}$ dans l'écriture de $P$.
\end{defi}

Ainsi, tout $q$-polynôme s'écrit $\displaystyle \sum_{i=0}^d \gamma_i(P)X^{q^i}$ pour une bonne valeur de $d$.

Pour décoder, on cherche un analogue à droite du théorème \ref{interpolation}. Pour cela, une façon naturelle de procéder est de chercher à transposer le résultat de ce théorème, ce qui fait intervenir une certaine notion de dualité. On introduit alors une forme bilinéaire symétrique convenable : Celle issue de la forme trace.


\begin{prop}
  La forme trace de $\Fqm/\Fq$ induit une forme bilinéaire, symétrique non dégénérée $$\fun{\inner{\cdot , \cdot}}{\Fqm\times\Fqm}{\Fq}{(a,b)}{Tr(ab)}$$
\end{prop}
\begin{proof}
  Pour $x\in\Fqm$ on note $\fun{\varphi_x}{\Fqm}{\Fqm}{y}{xy}$. Si $x\neq 0$, $\varphi_x$ est une bijection de $\Fqm$ dans lui-même. En particulier $\#\{xy\mid y\in\Fqm\} = q^m$. Or, $Tr$ est un polynôme de degré $q^{m-1}$, donc ne peut pas s'annuler sur tout $\Img \varphi_x$. En particulier, $\Fqm^\perp = \{0\}$.
\end{proof}

\begin{prop}
  Quel que soit $q$, il existe une $\Fq$-base de $\Fqm$ orthonormale pour $\inner{\cdot ,\cdot}$.
\end{prop}

\begin{proof}
  Voir les notes d'Alain.
\end{proof}

Ces deux propositions nous permettent de parler d'adjoint pour la forme bilinéaire sus-mentionnée.

\begin{defi}
  Soit $f:\Fqm\rightarrow\Fqm$ un endomorphisme. L'adjoint de $f$ est l'endomorphisme $f^\star$ défini par, pour tous $(a,b)\in\Fqm^2, \inner{a, f(b)} = \inner{f^\star(a), b}$.
\end{defi}

\todo[inline]{C'est là qu'intervient le $m=n$. On parle d'adjoint d'un \textbf{endomorphisme}}.

De façon standard, l'adjoint existe et est unique.

\begin{prop}
  Soit $d\le m$ et soit $P:=\sum_{i=0}^d a_iX^{q^i}$ un $q$-polynôme de $q$-degré $d$. Alors son adjoint est le $q$-polynôme $$P^\star := \sum_{i=0}^d a_i^{q^{m-i}}X^{q^{m-i}}$$
\end{prop}

\begin{proof}
  Calcul direct.
\end{proof}

Et puisque la forme bilinéaire Trace est non dégénérée, on a la propriété standard suivante :

\begin{prop}\ \\
  \begin{itemize}
  \item $\Img(P)^\perp = \Ker(P^\star)$
  \item $\Ker(P)^\perp = \Img(P^\star)$
  \end{itemize}
\end{prop}

Revenons alors au problème de décodage. Il existe un résultat dual au théorème \ref{interpolation} :

\begin{thm}\label{interpolation droite}
  Soit $E$ un $q$-polynôme de rang $t$. Alors il existe un $q$-polynôme $V$ de $q$-degré $\le t$ tel que $EV=0$. De plus, $\gamma_0(V) = 1$.
\end{thm}

Pour cela, commençons par prouver la proposition plus faible suivante

\begin{prop}\label{interpolation droite faible}
  Soit $E$ un $q$-polynôme de rang $t$. Il existe un $q$-polynôme $V$ de $q$-degré $\le t$ tel que $EVX^{q^{m-t}} = 0$. De plus, $\gamma_0(V) = 1$.
\end{prop}
\begin{proof}
  On cherche un $q$-polynôme $P$ tel que $\Ker(E) \supset \Img(P)$, ie $\Img(E^\star) \subset \Ker(P^\star)$. $E$ est de rang $t$ donc il en est de même pour $E^\star$. Soit $Q$ le $q$-polynôme unitaire de $q$-degré $t$ dont les racines sont exactement $\Img(E^\star)$. $Q$ est de la forme $\displaystyle \sum_{i=0}^t a_i X^{q^i}$ avec $a_t = 1$. On pose $P = Q^\star$. Alors

  \begin{align*}
    P & = \sum_{i=0}^t a_i^{q^{m-i}}X^{q^{m-i}} \\
      & = \sum_{i=0}^t a_i^{q^{m-i}}X^{q^{t-i + m -t}} \\
      & = \left(\sum_{i=0}^t a_i^{q^{m-i}}X^{q^{t-i}}\right) \circ X^{q^{m-t}}\\
      & = \underbrace{\left(\sum_{i=0}^t a_{t-i}^{q^{m-t+i}}X^{q^{i}}\right)}_{V} \circ X^{q^{m-t}}\\
  \end{align*}
  $\Img(E^\star) = \Ker(Q)$ donc $\Ker(E) = \Img(P)$. On en déduit que $E\circ P = 0$ ou encore $E\circ V\circ X^{q^{m-t}}=0$, et $V$ est bien de $q$-degré $\le t$. De plus, $\gamma_0(V) = a_t = 1$.
\end{proof}

\todo[inline]{On a $\deg_q(V) = t - val_q(Q)$ où $val_q$ est l'indice du terme de plus petit degré apparaissant dans l'écriture de $Q^\star$. Sorte de « multiplicité » de $0$. A-t-on des infos dessus ? Ça serait intéressant dans le cas $n<m$ pour pouvoir contrôler le $q$-degré de l'annulateur à droite.}\ \\

Introduisons alors un opérateur de conjugaison :

\begin{important}
  Sage prend l'inverse comme convention : \emph{conjugate(-n)}\ \\

\begin{lstlisting}
sage: k.<t> = GF(5^3)
sage: Frob = k.frobenius_endomorphism()
sage: S.<x> = k[x, Frob]
sage: A = x^2 + t*x + t^2
sage: B = x^2 + Frob(t)*x + Frob(t^2)
sage: A*x^2 == x^2*B
True
sage: B == A.conjugate(-2)
True
\end{lstlisting}\ \\

\todo[inline]{Prendre la même définition que Sage ?}
\end{important}

\begin{defi}
  Soit $A := \sum_{i=0}^d \alpha_i X^{q^i}$ un $q$-polynôme. Pour $j\le m$, on appelle $j$-conjugué de $A$ et on note $\theta_j(A)$ le $q$-polynôme $$\theta_j(A) := \sum_{i=0}^d \alpha_{i}^{q^{m-j}}X^{q^i}.$$
\end{defi}

Il est intéressant de noter que $\theta_j$ est involutive, et conserve le $q$-degré. Par ailleurs, le $j$-conjugué de $A$ vérifie la relation suivante :

\begin{prop}
  $A\circ X^{q^j} = X^{q^j}\circ \theta_j(A)$
\end{prop}

\begin{proof}
  Soit $\displaystyle A :=\sum_{i=0}^d a_i X^{q^i}$ un $q$-polynôme.  Alors
  \begin{align*}
    A\circ X^{q^j} & = \sum_{i=0}^d a_i X^{q^{i+j}} \\
                   & = \sum_{i=0}^d \left(a_i^{q^{m-j}} X^{q^{i}}\right)^{q^j} \\
                   & = X^{q^j}\circ \theta_j(A)
  \end{align*}
\end{proof}

En particulier, on en déduit la propriété suivante

\begin{thm}
  Soit $A$ un $q$-polynôme. Pour tout $j$, $\theta_j(A)$ est un $q$-polynôme de même $q$-degré et de même rang que $A$.
\end{thm}

\begin{proof}
  On a déjà fait la remarque pour le $q$-degré. Pour le rang, il suffit de remarquer que les monômes sont inversibles : Ils représentent des isomorphismes de $\Fqm$ vu comme $\Fq$ espace vectoriel. On a alors la relation $$\theta_j(A) = X^{q^{m-j}} \circ A \circ X^{q^j}.$$ En particulier, $A$ et $\theta_j(A)$ sont \emph{équivalents}. Ils ont donc le même rang. Dit autrement, l'application $P\mapsto X^{q^{m-j}}\circ P \circ X^{q^j}$ est une isométrie pour la métrique rang.
\end{proof}

On peut enfin en déduire une preuve du théorème \ref{interpolation droite}.

\begin{proof}[Preuve du théorème \ref{interpolation droite}]
  Soit $E$ un $q-$polynôme de rang $t$, et posons $$\overline{E} := \theta_{m-j}(E).$$ C'est un $q$-polynôme de même $q$-degré et de même rang $t$ que $E$. D'après la proposition \ref{interpolation droite faible}, on peut trouver un $q$-polynôme $V$ de $q$-degré $\le t$ tel que $$\overline{E}VX^{q^{m-j}} = 0$$ Cette relation se réécrit alors à l'aide de l'opérateur de conjugaison :
  $$X^{q^{m-j}}\circ \theta_{m-j}(\overline{E})\circ \theta_{m-j}(V) = 0$$

  ou encore

  $$X^{q^{m-j}}\circ \left(E \circ \theta_{m-j}(V)\right) = 0$$ en utilisant le caractère involutif de $\theta_{m-j}$.

  En particulier, on a bien $$E \circ \theta_{m-j}(V) = 0$$ où $\theta_{m-j}(V)$ est un $q$-polynôme de $q$-degré $\le t$. De plus, $$\gamma_0(\theta_{m-j}(V)) = \gamma_0(V)^{q^{m-j}} = 1.$$
\end{proof}



Revenons enfin au problème de décodage : On dispose à présent de trois $q$-polynômes $Y, C, E$ qui interpolent respectivement le mot reçu, le mot de code et l'erreur ; avec $\deg_q(C) < k$ et $rg(E) = t$, et $$Y = C + E$$


D'après le théorème \ref{interpolation droite}, on dispose d'un $q$-polynôme $V$ de $q$-degré $\le t$ tel que $E\circ V = 0$ et $\gamma_0(V) = 1$. On a donc la relation suivante :

$$0 = E\circ V = Y\circ V - C\circ V $$

En évaluant en chacun des $g_i$, on en déduit le système suivant de $n$ équations
\begin{align}
  (Y\circ V)(g_i) = (C\circ V)(g_i)\mbox{ pour } i=1,\dots, n. \label{Syst1}
\end{align}

Or,

\begin{itemize}
\item $Y$ est connu, par interpolation.
\item $g_i$ sont connus.
\end{itemize}

On cherche $(V, C)$ solution de \ref{Syst1} vérifiant

\begin{itemize}
\item $C$ de $q$-degré $<k$
\item $V$ de $q$-degré $\le t$
\end{itemize}

Comme dans le cas du décodage à gauche, il s'agit d'un système quadratique, mais nous pouvons le linéariser, en posant $N := C \circ V$, pour obtenir

\begin{align}
  (Y\circ V)(g_i) = N(g_i)\mbox{ pour } i=1,\dots, n. \label{Syst2}
\end{align}

avec

\begin{itemize}
\item $N$ de $q$-degré $\le k+t-1$
\item $V$ de $q$-degré $\le t$
\end{itemize}


\ref{Syst2} est un système linéaire de $n$ équations et dont les inconnues sont les coefficients de $V$ (degré $\le t$ mais dont on connaît un coefficient, donc $t$ inconnues), et les coefficients de $N$ de degré au plus $k+t-1$ : Il y a donc $k+2t$ inconnues. On remarque que si $t \le (n-k)/2$, alors on dispose de plus d'équations que d'inconnues, donc on peut espérer résoudre le système. De plus, ce système linéaire est lié à \ref{Syst1} par la proposition suivante

\begin{prop}
  Une solution $(V,f)$ de \ref{Syst1} donne une solution $(V,f\circ V)$ de \ref{Syst2}.
\end{prop}

De plus, on a le résultat suivant

\begin{thm}
  On suppose que $t\le \frac{n-k}{2}$ et $E$ de rang $\le t$. Si $(V,N)$ est une solution différente de $(0,0)$ de \ref{Syst2}, alors $N=f\circ V$ où $(V,f)$ est solution de \ref{Syst1}.
\end{thm}

\begin{proof} Soit $(V, N)$ une solution de \ref{Syst2} différente de $(0,0)$, et soit $f$ de $q$-degré $<k$ interpolant le mot de code. Posons $R := N - f\circ V$. $$\deg_q(f\circ V) \le k - 1 + t$$ et $$\deg_q(N) \le k + t -1,$$ par conséquent $R$ est un $q$-polynôme, de $q$-degré $\le k+t-1$. Raisonnons par l'absurde, et supposons que $R\neq 0$. La dimension de son noyau est relié à son degré par la relation \begin{align}\dim\Ker(R) \le \deg_q(R). \label{degker}\end{align} \\

  Le sytème (\ref{Syst2}) donne $(Y\circ V)(g_i) = N(g_i)$ donc pour $i=1\dots n$, $$\underbrace{(Y-f)}_{=E}\circ V(g_i) = \underbrace{(Y\circ V)(g_i)}_{=N(g_i)} - (f\circ V)(g_i) = (N-f\circ V)(g_i) = R(g_i)$$ ou encore \begin{align}(E\circ V)(g_i) = R(g_i)\mbox{ pour }i=1,\dots, n\end{align}

  Par ailleurs, la famille $(g_i)_{i=1,\dots, n}$ est de rang $n$, donc \begin{align}n\le m.\end{align}

  \todo[inline]{On a encore le cas $n=m$ ici}

  \begin{itemize}
    \item Dans le cas $n=m$, la famille $(g_1, \dots, g_n)$ forme une base de $\Fqm$ vu comme $\Fq$-espace vectoriel, et par suite $$E\circ V = R.$$ En particulier, $R$ est de rang au plus $t \le (n-k)/2$.

      Par le théorème du rang, on en déduit que $$m \le \deg_q(R) + rg(R) \le k+t-1 + t = k+2t -1 \le n-1 < m$$ ce qui est absurde. Par conséquent, $N-f\circ V = 0$ ou encore $N = f\circ V$.

    \item Dans le cas $n\neq m$, on a en fait $n<m$. Donc on a tout de même une contradiction ici. Ce qui n'est pas clair, c'est ces histoires d'adjoints : On pourrait passer à la transposition et voir ce qui change ; mais aussi ces histoires de restriction à l'espace engendré par le support du code ...
    \end{itemize}
\end{proof}


En conclusion, si $t\le \frac{n-k}{2}$, alors toute solution $(V,N) \neq (0,0)$ permet de retrouver $f$ par division euclidienne à droite de $N$ par $V$.


\vspace{2\baselineskip}

\todo[inline]{Essayer de conclure dans le cas $n < m$}

\vspace{\baselineskip}

Dans ce cas, si le rang de la restriction de $E$ à l'espace engendré par le support du code est bien $t$, le rang de $E$ est lui borné \emph{inférieurement} par $t$, donc le degré de $V$ n'est pas clairement contrôlé ! En particulier, il pourrait très bien dépasser la capacité de correction ....\\

En effet, une autre façon de voir est d'aborder un point de vue matriciel : Soit $\mathcal{B}$ une base de $\Fqm$ vu comme $\Fq$-espace vectoriel. On note $T \in \M_{m\times n}(\Fq)$ la matrice dont les colonnes sont la décomposition des $g_i$ dans la base $\mathcal{B}$. Pour $P$ un polynôme linéaire, on note $M_P \in \M_m(\Fq)$ sa matrice dans la base $\mathcal{B}$. La relation entre le mot reçu, le mot de code et l'erreur peut donc s'écrire $$M_YT = M_fT + M_ET$$ ou encore $$(M_Y - M_f)T = M_ET$$

  L'algorithme de Loidreau revient à chercher un annulateur à gauche de l'erreur : On cherche $V$ tel que $$M_V(M_Y - M_f)(T) = 0$$ il suffit de chercher $V$ tel que $$M_V(M_Y - M_f) = 0$$

  Par ailleurs, $T$ est de rang $n$, le rang de ses colonnes. Lorsque $m=n$, $T$ est inversible, et les deux égalités précédentes sont en réalité équivalentes. Toujours dans le cas où $m=n$, ce qu'on a fait c'est en réalité passer à la transposition dans la relation pour obtenir $V$ tel que $$(M_Y^t - M_f^t)M_V^t = 0$$ et remarquer que la transposition nous redonnait la matrice d'un polynôme linéaire, et qu'on pouvait le factoriser par un isomorphisme pour obtenir un polynôme de même degré (et rang). \\

  Dans le cas où $n<m$, $T$ est juste \emph{inversible à gauche}, pas à droite, et le rang de $M_E$ est \emph{supérieur} à $t$. Par ailleurs, en restreignant on perd la structure de corps ...

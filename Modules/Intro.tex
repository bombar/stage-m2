\subsection{Quelques notations}

Soit $q$ une puissance d'un nombre premier. On note $\Fq$ le corps fini à $q$ éléments. On notera $F_q^{m\times n}$ l'ensemble des matrices à $m$ lignes et $n$ colonnes, et à coefficients dans $\Fq$. Sauf contreindication, on notera aussi $\Fq^n := \Fq^{1\times n}$ l'ensemble des vecteurs lignes à coordonnées dans $\Fq$. Dans la suite, pour $x\in\Fq$ on notera $x^{[k]} := x^{q^k}$, qu'on étend aux éléments de $\Fq^{m\times n}$ : Si $M = \begin{pmatrix}M_{1,1}&\dots & M_{1,n}\\\vdots & \ddots & \vdots \\ M_{m,1} & \dots & M_{m,n}\end{pmatrix}$ alors on pose $$M^{[k]} := \begin{pmatrix}M_{1,1}^{[k]}&\dots & M_{1,n}^{[k]}\\\vdots & \ddots & \vdots \\ M_{m,1}^{[k]} & \dots & M_{m,n}^{[k]}\end{pmatrix}.$$


\subsection{Métrique rang}

Soit $\Fqm/\Fq$ une extension de $\Fq$ de degré $m$. Soit $\mathcal{B} := (\beta_1, \dots, \beta_m)$ une base de l'extension. On définit alors l'application d'extension
\[
  ext_{\mathcal{B}} : \left\{\begin{array}{lll}
                               \Fqm & \rightarrow & \Fq^{m\times 1}\\
                               x = \sum_{i=1}^m x_i\beta_i & \mapsto & (x_1, \dots, x_m)^T
                             \end{array}\right.
\]

On étend cette application aux éléments de $\Fqm^n$ en posant :

\[
  ext_{\mathcal{B}} : \left\{\begin{array}{lll}
                               \Fqm^n & \rightarrow & \Fq^{m\times n}\\
                               x = (x_1, \dots, x_n) & \mapsto &
                                                                 \begin{pmatrix}
                                                                   ext_{\mathcal{B}}(x_1) & \mid & \dots & \mid & ext_{\mathcal{B}}(x_n)
                                                                 \end{pmatrix} =

                                                                 \begin{pmatrix}
                                                                   X_{1,1} & \dots & X_{1, n} \\
                                                                   \vdots & \ddots & \vdots \\
                                                                   X_{m,1} & \dots & X_{m,n}\\
                                                                 \end{pmatrix}
                             \end{array}\right.
\]

où $x_j = \sum_{i=1}^m X_{i,j}\beta_i$.


\begin{prop}
  Le rang de $ext_{\mathcal{B}}(x)$ est indépendant du choix de la base $\mathcal{B}$.
\end{prop}

Pour $x\in\Fqm^n$ on note $rk_q(x)$ ou $\Vert x\Vert_q$ ou encore $\Vert x \Vert$ lorsque le contexte est clair, le rang commun de $ext(x) \in \Fq^{m\times n}$

\begin{thm}
  $\Vert\dot\Vert$ induit une métrique sur $\Fqm^n$ appelée métrique rang.
\end{thm}

\begin{rem}
  Pour une même matrice $A\in\Fqm^{l\times n}$, les $\Fq$ et $\Fqm$ rangs peuvent être différents mais on a toujours $$rg_{\Fqm}(A) \le rg_{\Fq}(A)$$ L'égalité a lieu ssi les coefficients de la matrice échelonnée en ligne de $A$ sont tous dans $\Fq$.
\end{rem}


Dans la suite les codes seront munis de cette métrique.

\begin{thm}
  Soit $A\in GL_m(\Fq)$ et $B\in GL_n(\Fq)$. Alors l'application $X\mapsto AXB$ est une isométrie en métrique rang. Si de plus $m=n$ alors $X\mapsto AX^TB$ est aussi une isométrie en métrique rang. Et ce sont les seules.
\end{thm}

On rappelle enfin la notion importante de support en métrique rang

\begin{defi}
  Soit $x\in\Fqm^n$. Le support de $x$ est défini comme le $\Fq$ espace vectoriel engendré par les lignes de sa représentation matricielle $ext(x)$.
\end{defi}

\begin{rem}\ \\
  \begin{itemize}
  \item Une matrice $A\in\Fqm^{l\times n}$ est inversible à droite ssi ses lignes sont linéairement indépendantes ssi $rg(A) = l$.
  \item Une matrice $A\in\Fqm^{l\times n}$ est inversible à gauche ssi ses colonnes sont linéairement indépendantes ssi $rg(A) = n$.
  \end{itemize}
\end{rem}

\subsection{Autour de la Trace}

Soit $\Fqm/\Fq$ une extension finie de $\Fq$.

\begin{lemma}
  Soit $x\in\Fqm$. Alors $X := x + x^{[1]} + \dots + x^{[m-1]}\in\Fq$
\end{lemma}

\begin{proof}
  Par linéarité du Frobenius on a : $$X^q = \left(\sum_{k=0}^{m-1}x^{[k]}\right)^q = \sum_{k=0}^{m-1}x^{[k+1]} = \sum_{k=1}^{m-1}x^{[k]} + x^{q^m} = \sum_{k=1}^{m-1}x^{[k]} + x = \sum_{k=0}^{m-1}x^{[k]} = X$$ Par suite, $X$ est laissé stable par le Frobenius, donc $X$ est dans le corps de base : $\Fq$.
\end{proof}

On définit alors l'application trace par

\[
  \fun{Tr_{\Fqm/\Fq}}{\Fqm}{\Fq}{x}{x + x^{[1]} + \dots + x^{[m-1]}}
\]

\begin{thm}
  La trace est une application linéaire, surjective, et $$\Ker(Tr) = \{x\in\Fqm\mid \exists a\in\Fqm, x = a^q -a\}$$
\end{thm}

\begin{proof}\ \\
  \begin{itemize}
  \item La linéarité vient de la linéarité du Frobenius.
  \item $T + T^{[1]} + \dots + T^{[m-1]}$ est un polynôme de degré $q^{m-1}$ donc a au plus $q^{m-1}$ racines. Or, $\#\Fqm = q^m$. Par suite il existe $x\in\Fqm, Tr(x) \neq 0$. La Trace est alors une forme linéaire non nulle, elle est donc surjective.
  \item Soit $$\fun{\Phi}{\Fqm}{\Fqm}{a}{a^q -a}.$$ Il s'agit de montrer que $\Ker(Tr) = \Img(\Phi)$. On a déjà $\Img(\Phi)\subset \Ker(Tr)$ d'après le lemme ci-dessus. Pour l'inclusion réciproque, on raisonne par dimension : $\Ker(\Phi) = \Fq$ donc par le théorème du rang $rg(\Phi) = m-1$. Par ailleurs, la trace est de rang 1, donc $\dim(\Ker(Tr)) = m-1$. D'où le résultat.
  \end{itemize}
\end{proof}


\begin{thm}[Delsarte]
  Soit $\mathcal{C}\subseteq \Fqm^n$ un code linéaire. Soit $\mathcal{C}_{\mid\Fq} := \mathcal{C}\cap\Fq$. On pose $Tr(\mathcal{C}) := \{(Tr(c_1), \dots, Tr(c_n))\mid (c_1, \dots, c_n)\in \mathcal{C}\}$. Alors $$(\mathcal{C}_{\mid\Fq})^\perp = Tr(C^\perp)$$
\end{thm}


On introduit enfin la notion de base duale d'une extension.

\begin{thm}
  Soit $(\gamma_1, \dots, \gamma_m)$ une base de l'extension $\Fqm/\Fq$. Alors il existe une unique base $(\gamma_1^*, \dots, \gamma_n^*)$ de $\Fqm/\Fq$ telle que pour tout $i,j$, $Tr(\gamma_i\gamma_j^*) = \mathds{1}_{i=j}$. En outre, si $x = \sum_{i=1}^m x_i \gamma_i \in \Fqm$ alors $x_i = Tr(x\gamma_i^*)$. $\gamma^*$ est appelée base duale de $\gamma$.
\end{thm}

\begin{proof}
  L'application $(x,y) \mapsto Tr(xy)$ est une forme bilinéaire, symétrique, non dégénérée. Soit $c_i : x = \sum_{i=1}^m x_i\gamma_i \mapsto x_i$ la projection sur $\Fq\gamma_i$. Alors $c_i$ est une forme linéaire. Alors il existe un unique $\gamma_i^*\in\Fqm$ tel que $c_i(x) = Tr(x\gamma_i^*)$. Alors :

  \begin{itemize}
  \item $Tr(\gamma_i\gamma_j^*) = c_j(\gamma_i) = \mathds{1}_{i=j}$
  \item S'il existe $\lambda_1, \dots, \lambda_m \in \Fq$ tels que $\sum_{i=1}^m \lambda_i\gamma_i^* = 0$ Alors par $\Fq$ linéarité, pour tout $j$ on a $$\sum_{i=1}^m \lambda_i Tr(\gamma_i^* \gamma_j) = 0$$ ie $\lambda_j = 0$. Par suite, $(\gamma_1^*, \dots, \gamma_m^*)$ est une famille libre, et donc forme une base de $\Fqm/\Fq$.
  \end{itemize}
\end{proof}


\subsection{Codes de Gabidulin}
Soit $\Gab := \Gab_k(\mathbf{g})$ le code de gabidulin de paramètres $[n, k, d]$ engendré par $\mathbf{g}\in\F_{q^m}$, c'est à dire dont la matrice génératrice est $$\begin{pmatrix} \\ \mathbf{g}^{[0]} \\ \\ \hline \\ \mathbf{g}^{[1]} \\ \\ \hline \\ \vdots \\ \\ \hline \\ \mathbf{g}^{[k-1]}\\ \ \end{pmatrix} \in \Fq^{k\times n}$$

\begin{thm}
  Les codes de gabidulin sont des codes MRD (Maximum Rank Distance), ie ils satisfont l'égalité dans l'inégalité de singleton : $d = n-k+1$. Munis de la métrique de Hamming, ils sont aussi MDS puisque $rg(x) \le w_H(x)$.
\end{thm}

\begin{thm}
  Le dual d'un gabidulin de dimension $k$ est encore un gabidulin, de dimension $n-k$. En particulier, le dual d'un gabidulin de paramètres $[n, k, n-k+1]$ est un gabidulin de paramètres $[n, n-k, k+1]$
\end{thm}

\begin{thm}
  Soit $T\in GL_n(\Fq)$. Alors le code $\Gab T$ est un code de gabidulin, engendré par $\mathbf{gT}$.
\end{thm}

En outre, les codes de gabidulin bénéficient d'un algorithme de décodage efficace jusqu'à la moitié de la distance minimale. On présentera un algorithme dû à Loidreau dans la section suivante, mais énonçons tout de même ce résultat dû à Wachter-Zeh et al.

\begin{thm}[2011 \cite{wachter-zehFastDecodingGabidulin2013}]
  Soit $G$ un gabidulin de paramètres $[n, k]$ et soit $t\le \lfloor \frac{n-k}{2}\rfloor$ et soit $e\in \Fqm^n$ tel que $rang_q(e) \le t$. Soit $x\in\Fqm^n$. On pose $y := xG + e$ le mot bruité. Alors il est possible de retrouver $x$ en $O(m^3\log(m))$ opérations dans le corps de base $\Fq$.
\end{thm}

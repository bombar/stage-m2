\subsection{Anneaux de Polynômes linéaires}

\subsubsection{Définition et première propriétés}


\begin{important}
  Dans Sage, les $q$-Polynômes sont implémentés comme un cas particulier des polynômes tordus (SkewPolynomials). Voir \href{https://doc.sagemath.org/html/en/reference/polynomial_rings/sage/rings/polynomial/skew_polynomial_element.html#sage.rings.polynomial.skew_polynomial_element.SkewPolynomial}{la documentation}.

  Exemple:
  \begin{lstlisting}
    sage: k.<t> = GF(5^3)
    sage: Frob = k.frobenius_endomorphism()
    sage: S.<x> = k[x, Frob]
    sage: S
    Skew Polynomial Ring in x over Finite Field in t
    of size 5^3 twisted by t |--> t^5
    sage: S.random_element()
    2*t*x^2 + (4*t + 3)*x + 3*t^2 + 4*t
  \end{lstlisting}

\end{important}


Les polynômes linéaires ont été étudiés par Ore dans les années 1930 \cite{oreContributionsTheoryFinite1934}, \cite{oreSpecialClassPolynomials1933}.

\begin{defi}
  On appelle Polynôme linéaire, ou encore $q$-polynôme, un polynôme de la forme $$P = p_0X + p_1X^q + \dots + p_tX^{q^t},\; p_i\in \Fqm,\; p_t \neq 0.$$ L'entier $t$ est appelé $q$-degré de $P$ et est noté $deg_q(P)$. On note $\Lev\Fqm[X]$ l'ensemble de $q$-polynômes à coefficients dans $\Fqm$.
\end{defi}

Puisque le Frobenius est linéaire sur $\Fqm$, les $q$-polynômes peuvent se voir comme des applications linéaires sur $\Fqm$ vu comme un $\Fq$ espace vectoriel. Si le produit classique de deux $q$-polynômes n'est pas forcément linéaire, la composition reste un $q$-polynôme. Par ailleurs, Ore a montré qu'ils possédaient une division euclidienne analogue à celle que l'on connaît dans le cadre des polynômes classiques.

\begin{defi}
  Soient $A,B$ deux polynômes linéaires. Alors
  \begin{itemize}
  \item \textbf{division à droite} $A$ admet un quotient à droite $Q$ par $B$ s'il existe un $q$-polynôme $R$ tel que $A = Q\circ B + R$ et $\deg_q(R) < \deg_q(B)$.
  \item \textbf{division à gauche} $A$ admet un quotient à gauche $S$ par $B$ s'il existe un $q$-polynôme $T$ tel que $A = B\circ S + T$ et $\deg_q(T) < \deg_q(B)$.
  \end{itemize}
\end{defi}

\begin{prop}
  Muni des lois $+$ et $\circ$, $\Lev\Fqm[X]$ est un  anneau non commutatif. De plus, il est euclidien à gauche et euclidien à droite.
\end{prop}

\begin{rem}
  Le facteur limitant algorithmiquement est le calcul de $A\circ B$ à partir des coefficients de $A$ et $B$. Il est réalisé en $O(kk')$ multiplications dans $\Fqm$ où $k$ et $k'$ sont les $q$-degrés de $A$ et $B$.
\end{rem}

\subsubsection{Structure des racines et interpolation}

La nature linéaire du $q$-polynôme permet à l'ensemble de ses racines de former un espace vectoriel : Il s'agit de son noyau en tant qu'application $\Fq$-linéaire. On a par ailleurs un lien entre la dimension de cet espace et le $q$-degré du polynôme.

\begin{prop}\label{racines d'un q-polynome}
  Soit $P$ un $q$-polynôme de $q$-degré $k\neq 0$. Alors l'ensemble de ses racines $V$ forme un espace vectoriel de dimension au plus $k$. De plus, il est possible de trouver une base des racines de $P$ en $O(km)$ opérations dans $\Fq$.
\end{prop}

On a même de façon plus surprenante le résultat suivant d'interpolation

\begin{thm}\label{interpolation}
  Soit $V$ un espace vectoriel de dimension $k$. Alors il existe un unique $q$-polynôme unitaire $P$ de $q$-degré $k$ tel que $V$ soit l'ensemble des racines de $P$. Si de plus on connaît une base de $V$, la construction de ce polynôme peut se faire en $O(k^2 + k\log(m))$ opérations dans $\Fq$.
\end{thm}

Une preuve de ce théorème pourra être trouvée dans \cite{muratResultantsPolynomesOre2014}.\ \\

Ce théorème nous permet d'écrire un analogue du célèbre théroème d'interpolation de Lagrange, pour les $q$-polynômes.

\begin{thm}\label{lagrange}
  Soit $(g_1, \dots, g_n) \in \Fqm^n$ de rang $n$. Soit $(y_1, \dots, y_n) \in \Fqm^n$. Il existe un unique $q$-polynôme $P$ de $q$-degré au plus $n-1$ tel que pour tout $i$, $P(g_i) = y_i$. Par ailleurs, on peut calculer $P$ en $O(n^3 + n^2\log(m))$ opérations dans $\Fq$.
\end{thm}
Il est intéressant de noter que dans le cas classique des polynômes en une indéterminée, on considérait une famille d'éléments distincts. L'analogue dans le cas des polynômes linéaires est la liberté de la famille.
\begin{proof}\ \\
  \begin{itemize}
  \item \textbf{Existence:} Soit $\tilde{P}_i$ l'unique $q$-polynôme unitaire de $q$-degré $n-1$ dont l'ensemble des racines est $V_i := \Vect(g_j\mid j\neq i)$. Alors $\tilde{P}_i(g_i) \neq 0$. Posons alors $$P_i := \tilde{P}_i(g_i)^{-1}\tilde{P}_i.$$

    La famille ainsi construite vérifie $P_i(g_j) = \mathds{1}_{i=j}$, et chaque $P_i$ peut être construit en $O(n^2 + n\log(m))$ opérations.


    On en déduit que le $q$-polynôme suivant convient $$P := \sum_{i=1}^n y_i P_i$$

  \item \textbf{Unicité:} Si $P$ et $Q$ vérifient tous les deux les hypothèses, alors $P-Q$ est un polynôme linéaire de $q$-degré au plus $n-1$ s'annulant sur tout $V = \Vect(g_1, \dots, g_n)$. Par liberté des $g_i$, $V$ est de dimension $n$. Le théorème \ref{racines d'un q-polynome} permet alors d'affirmer que $P-Q = 0$ ou encore $P = Q$.
  \end{itemize}
\end{proof}

\subsubsection{Reconstruction de $q$-polynômes}


Avec ce formalisme, les codes de Gabidulin peuvent se voir comme un analogue en métrique rang des codes de Reed-Solomon : Soit $g=(g_1,\dots,g_n)\in\Fqm^n$ de rang $n$. Le code de Gabidulin $\Gab_k(g)$ engendré par $g$ est le code d'évaluation des $q$-polynômes de $q$-degré strictement inférieur à $k$ :

$$\Gab_k(g) = \{ (f(g_1),\dots,f(g_n)) \mid f\in\Lev\Fqm[X]_{<k}\}.$$

On introduit alors naturellement le problème suivant dit de reconstruction de polynômes linéaires présenté par Loidreau dans \cite{loidreauWelchBerlekampAlgorithmDecoding2005}, et on rappelle le problème de décodage borné.

\paragraph{Reconstruction de Polynômes Linéaires (RPL)}
\begin{description}
\item[DONNÉES] $\mathbf{y}=(y_1,\dots,y_n)\in\Fqm^n$, $\mathbf{g}=(g_1, \dots, g_n)\in\Fqm^n$ de rang $n$, $k, t$
\item[PROBLÈME] Trouver l'ensemble des couples $(V, f)$ où $V$ est un $q$-polynôme non nul de $q$-degré $\le t$ et où $f$ est un $q$-polynôme de $q$-degré $<k$, tel que $$V(y_i) = V[f(g_i)].$$
\end{description}

\paragraph{Décodage des codes de Gabidulin}
\begin{description}
\item[DONNÉES] $\mathcal{G} = \Gab_k(\mathbf{g})\subset \Fqm^n$ un code de Gabidulin, $\mathbf{y}=(y_1,\dots,y_n)\in\Fqm^n$ et $t$
\item[PROBLÈME] Trouver l'ensemble des $\mathbf{c}\in\Gab_k(\mathbf{g})$ tel qu'il existe $\mathbf{e}\in\Fqm^n$ de rang $\le t$ tel que $$\mathbf{y} = \mathbf{c} + \mathbf{e}.$$
\end{description}

Le décodage des codes de Gabidulin est un cas particulier, de plus le problème de la reconstruction des polynômes linéaires est supposé difficile au-delà de la capacité de correction du code de Gabidulin associé, ce qui justifie son utilisation en cryptographie.

\begin{prop}
  Le problème de reconstruction de polynôme linéaire est polynomialement équivalent au problème de décodage des codes de Gabidulin.
\end{prop}

\begin{proof}
  Soit $(\mathbf{y}, \mathbf{g}, k, t)$ une instance de RPL. Considérons le code de Gabidulin $\mathcal{G} = \Gab_k(\mathbf{g})$. Alors $(\mathcal{G}, \mathbf{y}, t)$ est une instance du problème de décodage. Soit $\mathbf{c}\in \mathcal{G}$ une solution. On dispose alors de $\mathbf{e}$ de rang $\le t$ tel que $$\mathbf{y} = \mathbf{c} + \mathbf{e}$$

  \begin{itemize}
  \item Soit $E := \Vect(e_1, \dots, e_n)$. Par hypothèse, $E$ est de dimension $d\le t$. D'après le théorème \ref{interpolation}, on peut construire en temps polynomial un $q$-polynôme $V$ de $q$-degré $d\le t$ et tel que $E$ soit exactement l'ensemble des racines de $V$. Par ailleurs, puisque $\mathbf{c} \in \Gab_k(\mathbf{g})$, on dispose d'un unique $q$-polynôme $f$ de degré $<k$ et tel que $c_i = f(g_i)$. $f$ peut être calculé en temps polynômial grâce au théorème \ref{lagrange}. Enfin on a pour tout $i$ : $$V(y_i) = V(c_i) + V(e_i) = V(c_i) = V[f(g_i)]$$ donc $(V, f)$ est une solution du problème de reconstruction.
  \item Réciproquement, soit $(V,f)$ une solution du problème de reconstruction. Posons $c_i := f(g_i)$. Alors puisque $f$ est de $q$-degré $<k$, $\mathbf{c} \in \Gab_k(\mathbf{g})$. Par ailleurs, on a pour tout $i$ $$V(y_i) = V[f(g_i)]$$ donc par linéarité $$V(y_i - f(g_i)) = 0.$$ Posons $e_i := y_i - f(g_i)$. Alors le rang de $\mathbf{e}$ est inférieur au $q$-degré de $V$, donc $e$ est de rang $\le t$. Enfin on a bien par construction $$\mathbf{y} = \mathbf{c} + \mathbf{e}.$$ donc $\mathbf{c}$ est une solution au problème de décodage.
  \end{itemize}
\end{proof}

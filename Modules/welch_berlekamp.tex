\subsection{Un algorithme de décodage à la Welch-Berlekamp}

Pour décoder un code de Gabidulin, on peut donc chercher à résoudre le problème de reconstruction de $q$-polynômes. Ce qui revient à résoudre le système

\begin{align}
  \left\lbrace \begin{array}{ll}
                 V(y_i) = V\circ P (g_i), & i=1,\dots, n \\
                 \deg_q(V) \le t & \\
                 \deg_q(P)<k & \\
               \end{array} \right. \label{WBQ}
\end{align}

dont les inconnues sont les coefficients de $V$ et $P$. C'est un système quadratique de $n$ équations à $k+t+1$ inconnues. Pour résoudre ce système, une méthode serait de rajouter les équations définissant le corps et de déterminer une base de Gröbner du système. On va plutôt utiliser une méthode de linéarisation, à la Welch-Berlekamp, et chercher à résoudre le système plus général

\begin{align}
  \left\lbrace \begin{array}{ll}
                 V(y_i) = N (g_i), & i=1,\dots, n \\
                 \deg_q(V) \le t & \\
                 \deg_q(N)\le k + t - 1 & \\
               \end{array} \right. \label{WBL}
\end{align}

C'est un système linéaire, dont les inconnues sont les coefficients de $N$ et $V$. Il y a $n$ équations, et $k + 2t + 1$ inconnues. Il est plus général, au sens de la proposition suivante

\begin{prop}
  Toute solution de (\ref{WBQ}) donne une solution de (\ref{WBL})
\end{prop}

\begin{proof}
  Soit $(V,p)$ une solution de (\ref{WBQ}). Posons $N = V\circ p$. Alors $(V, N)$ est une solution de (\ref{WBL}).
\end{proof}

Si l'espace des solutions de (\ref{WBL}) est de dimension $0$, on en déduit qu'il n'y a pas de mot de code à distance inférieure ou égale à $t$. Mais on a mieux :


\begin{thm}
  Si $t \le (n-k)/2$ et si $\mathbf{e}$ est de rang $\le t$ et vérifie $\mathbf{y} = P(\mathbf{g}) + \mathbf{e}$ alors pour toute solution $(V, N) \neq (0,0)$ du système \ref{WBL} on a $N = V\circ P$.
\end{thm}

\begin{proof}
  Soit $(V, N)$ une solution non nulle de (\ref{WBL}). Par hypothèse, pour tout $i$ on a $$y_i = P(g_i) + e_i$$ et donc $$V(y_i) = (V\circ P)(g_i) + V(e_i).$$ Par ailleurs, par définition de $(V, N)$ on a $$V(y_i) = N(g_i)$$ et donc $$(N-V\circ P)(g_i) = V(e_i).$$

  Supposons que $N - V\circ P \neq 0$. C'est un $q$-polynôme de $q$-degré au plus $k+t-1$. Considérons sa restriction $Q$ à $\Vect(g_1, \dots, g_n)$. Alors $$rg((N - V\circ P)(g_1), \dots, (N - V\circ P)(g_n)) = rg(Q) = n - \dim \Ker Q$$ d'après le théorème du rang et la liberté de $(g_1, \dots, g_n)$. Or, $$\Ker Q \subset \Ker(N-V\circ P)$$ donc $$\dim \Ker Q \le \deg_q(N-V\circ P) \le k+t-1$$ et finalement $$rg((N - V\circ P)(g_1), \dots, (N - V\circ P)(g_n)) \ge n - (k+t) + 1$$ d'où $$rg(V(e_1), \dots, V(e_n)) \ge n - (k+t) + 1$$

  D'autre part, $rg(V(e_1), \dots, V(e_n)) \le rg(\mathbf{e}) \le t$ d'où $$t \le n - (k+t) + 1$$ ou encore $$t > \dfrac{n-k}{2}$$ ce qui contredit l'hypothèse. Donc $N = V\circ P$.
\end{proof}

Autrement dit, jusqu'à la moitié de la distance minimale, pour décoder il suffit de considérer n'importe quelle solution $(V,N) \neq (0,0)$ de (\ref{WBQ}) et de faire la division euclidienne à gauche de $N$ par $V$ pour retrouver $P$.

# Choose parameters
q = 2
m = 41
n = m


def ext_inv(Fqm,Fq,mat):
    # Maps the matrix mat over Fq to a vector over Fqm
    m = Fqm.degree()
    output = sum([mat[i,:]*(Fqm.gen()^i) for i in range(m)])
    return output

def rand_vec(Fqm,Fq,rk,leng):
    # Generate a random vector of length leng over Fqm with rank rk over Fq
    m = Fqm.degree()
    # 1. Compute echelonizable matrix with rank rk
    rank_false = true
    while rank_false:
        A_mat = matrix.random(Fq,m,rk)
        if A_mat.rank()==rk:
            rank_false = false
    print("A =\n{}".format(A_mat))

    rank_false = true
    while rank_false:
        B_mat = matrix.random(Fq,rk,leng)
        if B_mat.rank()==rk:
            rank_false = false
    print("B =\n{}".format(B_mat))

    out_mat = A_mat*B_mat
    out_vec = ext_inv(Fqm,Fq,out_mat)

    return out_vec


# Define all fields:
Fq = GF(q)
Fqm.<a> = GF(q^m,modulus='primitive')

# ----------- Key Generation -------------------
# 1. Choose g at random with rank(g)= n
g = rand_vec(Fqm,Fq,rk=n,leng=n)


from skew_polynomials import adjoint, reduction

q = 2^2
m = 5

d1 = m
d2 = m

Fqm = GF(q^m)
twist = Fqm.frobenius_endomorphism()
S = Fqm['x', twist]


A = reduction(S.random_element(degree=(0,d1)))
B = reduction(S.random_element(degree=(1,d2)))

Adj_A = adjoint(A, m)
Adj_B = adjoint(B, m)

z10 = Fqm.gen()


N = list(reversed([1, z10^9 + z10^6 + z10^4 + z10^3 + z10^2 + z10 + 1, z10^8 + z10^6 + z10^3 + z10^2, 0, 0, 0]))
N = S(N)

# print(N)

Nstar = adjoint(N, m, verbose=True)

print(Nstar)

exit()

# print(z^4)
# A4 = S([z]*(m+2))
# print(f"A4 = {A4}")
# print(f"On réduit :\nA4={reduction(A4, m, verbose=False)}")


# A4 = S([0, z^2, z, 1, z^2+z])
# rA4 = reduction(A4, m, False)

# Adj_A4 = adjoint(A4, m, verbose=False)

# print(f"A4    = {A4}, on projette:\nA4    = {rA4}, on transpose:\n(A4)* = {Adj_A4}")

# exit()

print("Check right quo rem")
assert (A*B).right_quo_rem(B) == (A, 0)

print("Checking Degrees :")
print(f"A is of q-degree {A.degree()} while Adj_A is of q-degree {Adj_A.degree()}")
print(f"Coefficients of A are {A.padded_list(m+1)} while coefficients of Adj_A are {Adj_A.padded_list(m+1)}")
print("\n")

print("Checking involution")

try:
    assert(adjoint(Adj_A, m) == A)
except AssertionError:
    print(f"(A*)* = {adjoint(Adj_A)};\nit should be {A}")
    raise

try:
    assert(adjoint(Adj_B, m) == B)
except AssertionError:
    print(f"(B)* = {adjoint(Adj_B)};\nit should be {B}")
    raise


print(".... Ok !\n")

print("Checking trace property")

x1, x2 = Fqm.random_element(), Fqm.random_element()
assert((A(x1)*x2).trace() == (x1*Adj_A(x2)).trace())

print(".... Ok !\n")



print(f"A*B = {A*B}")
print(f"Une fois projeté,\nA*B = {reduction(A*B, m, verbose=True)}")

Adj_AB = adjoint(A*B, m)
# AdjB_AdjA = (Adj_B*Adj_A) >> m
# AdjB_AdjA_shifted = Adj_B*(Adj_A.shift(-m))

AdjB_AdjA = reduction(Adj_B*Adj_A, m)
AdjB_AdjA_shifted = Adj_B*(Adj_A)


# print(f"Adj_AB = {Adj_AB}")

# print(f"Adj_A = {Adj_A}")
# print(f"Adj_B = {Adj_B}")
# print(f"Adj_B x Adj_A = {(Adj_B*Adj_A).coefficients()}")


# print("Checking shift associativity")
# try:
#     assert(AdjB_AdjA == AdjB_AdjA_shifted)
#     print(".... Ok !\n")
# except AssertionError:
#     print("Error !")
#     print(f"AdjB_AdjA == {AdjB_AdjA}")
#     print(f"while AdjB_AdjA_shifted == {AdjB_AdjA_shifted}")
#     raise


print("Checking anticommutativity")


# assert(len(Adj_AB.coefficients()) == len(AdjB_AdjA.coefficients()))

try:
    assert(Adj_AB.degree() == (AdjB_AdjA).degree())
except:
    print("Error !")
    print(f"(AB)* is of q-degree {Adj_AB.degree()}\n")
    print(f"While (B*)(A*) is of q-degree {(AdjB_AdjA).degree()}\n")
    print(f"(AB)* = {Adj_AB}")
    print(f"(B*)(A*) = {AdjB_AdjA}")
    print(f"Lorsque non projeté, (B*)(A*) = {(Adj_B)*(Adj_A)}")
    print(f"Lorsque non projeté, (B*)(A*) = {(Adj_B)*(Adj_A)}")
    print(f"Coefficients of (AB)* are {Adj_AB.coefficients()}")
    print(f"Coeffs of (B*)(A*) are {(AdjB_AdjA).coefficients()}")
    raise
try:
    assert(Adj_AB.coefficients() == (AdjB_AdjA).coefficients())
except:
    print("Error !!")
    print(f"Coefficients of (AB)* are {Adj_AB.coefficients()}\n")
    print(f"Coefficients of (B*)(A*) are {(AdjB_AdjA).coefficients()}\n")
    raise

print(".... Ok !\n")

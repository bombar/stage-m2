from skew_polynomials import reduction, adjoint
from sage.matrix.constructor import matrix
from sage.matrix.special import block_matrix
from sage.modules.free_module_element import vector
from copy import copy


def attack_repaired_fl(k_pub_vec, g, Fqm, k, w):
    """
    k_pub_vec are the u coordinates of k_pub in Fqmu/Fqm.
    It is a list of length u, each component being a list of length n,
    reprensenting a noisy word.
    g = g1, ..., gn is the support of the underlying gabidulin code
    """

    print(f"Fqm = {Fqm}")
    m = Fqm.degree()
    u = len(k_pub_vec)
    n = len(g)
    print(f"g = {g}")
    for i in range(u):
        assert n == len(k_pub_vec[i])
    t = (u * (n - k)) // (u + 1)
    s = Fqm.frobenius_endomorphism()
    R = Fqm["x", s]  # skew polynomials
    points = []
    print(k_pub_vec[0])
    print(f"m={m}, n={n}, k={k}, (n-k)/2={(n-k)//2}, t={t}, u={u}, w={w}")
    for i in range(u):
        for j in range(n):
            xx = k_pub_vec[i][j]
            assert xx == xx.matrix().row(0)[0]
    points = [
        [(g[j], k_pub_vec[i][j].matrix().row(0)[0]) for j in range(n)] for i in range(u)
    ]
    # for i in range(u):
    #     print(points)
    Ys = [reduction(R.lagrange_polynomial(points[i]), m) for i in range(u)]
    Ys_star = [adjoint(Y, m) for Y in Ys]
    rstar_s = [Ystar.multi_point_evaluation(g) for Ystar in Ys_star]
    # for i in range(u):
    #     print(rstar_s[i])
    l0 = k + t - 1
    l1 = t
    G = matrix(Fqm, n, k + t, lambda i, j: (s ** (m - j))(g[i]))
    assert G.nrows() == n and G.ncols() == k + t
    assert G.rank() == k + t
    Ls = [
        matrix(Fqm, n, t + 1, lambda i, j: (s ** (m - j))(rstar_s[l][i]))
        for l in range(u)
    ]
    assert all([Ls[i].nrows() == n for i in range(u)])
    assert all([Ls[i].ncols() == t + 1 for i in range(u)])
    assert len(Ls) == u
    for i, M in enumerate(Ls):
        print(f"Y_{i} is of rank {M.rank()}")
    # Ss = [[matrix(Fqm, n, t+k)] * (u + 1)] * u
    Ss = []
    # Ss = [[0] * (u + 1)] * u
    # print("yoloswag\n\n")
    for i in range(u):
        Si = [matrix(Fqm, n, k + t)] * (u + 1)
        Si[u] = -Ls[i]
        Si[i] = copy(G)
        Ss.append(Si)
    # print(Ss)
    S = block_matrix(Fqm, u, u + 1, Ss)
    # print(S)
    print(f"S has {S.nrows()} rows and {S.ncols()} columns")
    assert S.nrows() == n * u
    assert S.ncols() == (t + k) * u + t + 1
    S = S.right_kernel()
    print(f"kernel of rank {S.rank()}")
    S = S.basis_matrix().row(0)
    Nstar_coeff_s = [
        list(reversed(S.list_from_positions(range(i * (l0 + 1), (i + 1) * (l0 + 1)))))
        for i in range(u)
    ]
    # print(f"Nstar_coeff_s has length {len(Nstar_coeff_s)}")
    assert len(Nstar_coeff_s) == u
    Vstar_coeff = list(
        reversed(S.list_from_positions(range(u * (l0 + 1), u * (l0 + 1) + t + 1)))
    )
    for N in Nstar_coeff_s:
        print(N)
    print(Vstar_coeff)
    Nstar_s = [R(N) << m - l0 for N in Nstar_coeff_s]
    Vstar = R(Vstar_coeff) << m - l1
    Ns = [adjoint(Nstar, m) for Nstar in Nstar_s]
    V = adjoint(Vstar, m)
    print(V)
    print(f"V is of degree {V.degree()}")
    res = []
    for N in Ns:
        res.append(N.right_quo_rem(V))
    print([x[1] == 0 for x in res])
    if all([x[1] == 0 for x in res]):
        print("Euclidean division worked !")
        for x in res:
            assert x[0].degree() < k
        codewords = [vector(x[0].multi_point_evaluation(g)) for x in res]
        return codewords


def attack2_repaired_fl(k_pub_vec, g, Fqm, k, w, R=None, t=None, zeta=None):
    """
    k_pub_vec are the u coordinates of k_pub in Fqmu/Fqm.
    It is a list of length u, each component being a vector of Fqm^n
    reprensenting a noisy word.
    g = g1, ..., gn is the support of the underlying gabidulin code
    """

    print(f"Fqm = {Fqm}")
    m = Fqm.degree()
    u = len(k_pub_vec)
    n = len(g)
    # print(f"g = {g}")
    for i in range(u):
        assert n == len(k_pub_vec[i])
    if not t:
        t = (u * (n - k)) // (u + 1)
    # t = (n-k) // 2
    if t < (n - k) // 2:
        raise Exception("Attack unsuccessful")
    if R:
        s = R.twist_map()
    else:
        s = Fqm.frobenius_endomorphism()
        R = Fqm["y", s]  # skew polynomials
    points = []
    print(f"m={m}, n={n}, k={k}, (n-k)/2={(n-k)//2}, t={t}, u={u}, w={w}")
    if zeta:
        print(f"theoretical max error rank is {(zeta*(n-k))//(zeta + 1)}")
    points = [[(g[j], k_pub_vec[i][j]) for j in range(n)] for i in range(u)]
    Ys = [reduction(R.lagrange_polynomial(points[i]), m) for i in range(u)]
    Ys_star = [adjoint(Y, m) for Y in Ys]
    rstar_s = [Ystar.multi_point_evaluation(g) for Ystar in Ys_star]
    l0 = k + t - 1
    l1 = t
    G = matrix(Fqm, n, k + t, lambda i, j: (s ** (m - j))(g[i]))
    assert G.nrows() == n and G.ncols() == k + t
    assert G.rank() == k + t
    Ls = [
        matrix(Fqm, n, t + 1, lambda i, j: (s ** (m - j))(rstar_s[l][i]))
        for l in range(u)
    ]
    assert all([Ls[i].nrows() == n for i in range(u)])
    assert all([Ls[i].ncols() == t + 1 for i in range(u)])
    assert len(Ls) == u
    for i, M in enumerate(Ls):
        print(f"Y_{i} is of rank {M.rank()}")
    # Ss = [[matrix(Fqm, n, t+k)] * (u + 1)] * u
    Ss = []
    # Ss = [[0] * (u + 1)] * u
    # print("yoloswag\n\n")
    for i in range(u):
        Si = [matrix(Fqm, n, k + t)] * (u + 1)
        Si[u] = -Ls[i]
        Si[i] = copy(G)
        Ss.append(Si)
    # print(Ss)
    S = block_matrix(Fqm, u, u + 1, Ss)
    # print(S)
    print(f"S has {S.nrows()} rows and {S.ncols()} columns")
    assert S.nrows() == n * u
    assert S.ncols() == (t + k) * u + t + 1
    SS = copy(S)
    S = S.right_kernel()
    print(f"kernel of rank {S.rank()}")
    try:
        S = S.basis_matrix().row(0)
    except IndexError:
        return attack2_repaired_fl(k_pub_vec, g, Fqm, k, w, R, t - 1, zeta)
    Nstar_coeff_s = [
        list(reversed(S.list_from_positions(range(i * (l0 + 1), (i + 1) * (l0 + 1)))))
        for i in range(u)
    ]
    # print(f"Nstar_coeff_s has length {len(Nstar_coeff_s)}")
    assert len(Nstar_coeff_s) == u
    Vstar_coeff = list(
        reversed(S.list_from_positions(range(u * (l0 + 1), u * (l0 + 1) + t + 1)))
    )
    # for N in Nstar_coeff_s:
    #     print(N)
    # print(Vstar_coeff)
    Nstar_s = [R(N) << m - l0 for N in Nstar_coeff_s]
    Vstar = R(Vstar_coeff) << m - l1
    res_left = []
    for N in Nstar_s:
        res_left.append(N.left_quo_rem(Vstar))
    Ns = [adjoint(Nstar, m) for Nstar in Nstar_s]
    V = adjoint(Vstar, m)
    # print(V)
    print(f"V is of degree {V.degree()}")
    res = []
    for N in Ns:
        res.append(N.right_quo_rem(V))
    print([x[1] == 0 for x in res])
    codewords = [vector([Fqm(0) for i in range(n)]) for x in res]
    if all([x[1] == 0 for x in res]):
        print("Euclidean division worked !")
        for x in res:
            assert x[0].degree() < k
        codewords = [vector(x[0].multi_point_evaluation(g)) for x in res]
    else:
        return attack2_repaired_fl(k_pub_vec, g, Fqm, k, w, R, t - 1, zeta)
    N = (Nstar_coeff_s, Nstar_s, Ns)
    V_ = (Vstar_coeff, Vstar, V)
    return codewords, SS, N, V_


def attack3_repaired_fl(Ts, c, g, k, d, Fqm, q=None, R=None):
    """
    Tk_pub = trace of the public key over Fqm. It is a vector of length n
    and coordinates in Fqm.

    c is the ciphertext

    g is the support of the gabidulin, k it's dimension

    d is the public rank of the small error in the ciphertext

    R is a ring of linear polynomial, because Sage crashes.
    """
    n = len(g)
    if not q:
        q = Fqm.characteristic()

    if R:
        s = R.twist_map()
    else:
        s = Fqm.frobenius_endomorphism()
        R = Fqm["y", s]  # skew polynomials

    # y = R.gen()

    Tinterp = []
    for Tk_pub in Ts:
        pointsT = list(zip(g, Tk_pub))
        T = reduction(R.lagrange_polynomial(pointsT))
        Tinterp.append(T)
    l0 = k + d - 1
    l1 = d
    G = matrix(Fqm, n, k + d, lambda i, j: (s ** (j))(g[i]))
    S = []
    S.append(G)
    for T in Tinterp:
        Tm = matrix(Fqm, n, d + 1, lambda i, j: (T(g[i])) ** (q ** j))
        S.append(Tm)
    Cm = matrix(Fqm, n, d + 1, lambda i, j: (s ** j)(c[i]))
    S.append(Cm)
    S = block_matrix(Fqm, 1, 2 + len(Ts), S)
    S2 = matrix(
        Fqm,
        n,
        l0 + l1 + l1 + 3,
        lambda i, j: (s ** j)(g[i])
        if j < (l0 + 1)
        else (
            (
                (T(g[i])) ** (q ** (j - (l0 + 1)))
                if j < l0 + l1 + 2
                else (s ** (j - (l0 + l1 + 2)))(c[i])
            )
        ),
    )
    if len(Ts) == 1:
        assert(S2 == S)
    print(f"S has {S.nrows()} rows and {S.ncols()} columns")

    S = S.right_kernel()
    print(f"Kernel of rank {S.rank()}")
    S = S.basis_matrix().row(0)
    print(len(S))
    print(l0)
    print(l1)
    N = R(S.list_from_positions(range(len(S)-(d+1))))
    V = R(S.list_from_positions(range(len(S)-(d+1), len(S))))
    f, rem = (-N).left_quo_rem(V)
    if rem == 0:
        return (f, N, V, S)
    else:
        print(S)
        raise

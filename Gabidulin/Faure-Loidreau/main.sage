reset()

from attack import attack_repaired_fl
from sage.coding.relative_finite_field_extension import RelativeFiniteFieldExtension
from sage.modules.free_module_element import vector

load("helper_functions.sage")

#set_random_seed(1234)

# Choose parameters
# Works with m=n=41, k=27
#     * u=5, w=3
#     * u=4, w=3
#     * u=3, w=4
#     * u=2, w=5
#     * u=1, w=7
q = 2
# m = 10
m = 41
u = 3
n = m
# k = 5
k = 27
w = 4
# w = 8

# if (n-k)<=w:
#     print("(n-k)<=w")
#     assert 0

# if w<=floor( (n-k)/2):
#     print("w<=floor( (n-k)/2)")
#     assert 0

# if floor( (n-w-k)/2 )<=0:
#     print("t_pub = 0")
#     assert 0

# Define all fields:
Fq = GF(q)
Fqm.<a> = GF(q^m,modulus='primitive')  # modulus="primitive" ensures that the generator `a` is a primitive element (ie a generator of the multiplicative group)
# Fqm = GF(q^m)  # modulus="primitive" ensures that the generator `a` is a primitive element (ie a generator of the multiplicative group)
Fqmu_tmp.<c> = GF(q^(m*u),modulus='primitive')
# Fqmu_tmp = GF(q^(m*u))
PR.<y> = Fqm[]

# Fqmu_tmp.<c> = Fqm.extension(u)

# Fqmu_ext = RelativeFiniteFieldExtension(Fqmu_tmp, Fqm)
# Fqmu = Fqmu_ext.absolute_field()

# print(Fqmu)

# assert Fqmu in Fields()
# assert Fqm.is_subring(Fqmu)
# print(Fqmu.degree())

c = Fqmu_tmp.gen()

min_poly_c_overFq = PR(c.minpoly())
poly_list = prime_factors(min_poly_c_overFq)
minimal_polynomial_beta = poly_list[0]
Fqmu = Fqm.extension(minimal_polynomial_beta) # Ensures Fqm.is_subring(Fqmu) == True. May be realized with relative_finite_field_extension

# print(Fqmu)
# assert Fqmu in Fields()
# assert Fqm.is_subring(Fqmu)
# print(Fqmu.degree())

# exit()


print("\n# ----------- Key Generation -------------------")
# 1. Choose g at random with rank(g)= n
g = rand_vec(Fqm,Fq,rk=n,leng=n)

# 2. Choose x at random such that the last u positions form a basis of Fqmu over Fqm
x_vec = block_matrix(Fqmu,[matrix.random(Fqmu,1,k-u),rand_vec(Fqmu,Fqm,rk=u,leng=u)],nrows=1,subdivide=False)

# 3. Choose s according to Section IV. Reparation of the FL System. (Here \xi is 1)
s1 = rand_vec(Fqm,Fq,w,w)
s = ext_inv(Fqmu,Fqm,matrix(Fqm,[[s1[0,jj] for jj in range(w)] for ii in range(u)]))

# 4. Choose an invertible matrix P at random
P = matrix.random(Fq,n,n)
while not (P.rank() == n):
    P = matrix.random(Fq,n,n)

# 5. Build generator matrix of Gab
G_gab = matrix(Fqm,[[g[0,jj]^(q^(ii)) for jj in range(n)] for ii in range(k)])

# 6. Generate vector z
z_vec = matrix(Fqmu, [list(s[0][:])+[0]*(n-w)])*P.inverse()

# 7. Generate k_pub
k_pub = x_vec*G_gab + z_vec

# 8. Compute t_pub
t_pub = floor( (n-w-k)/2 )

print("Key Generation done")

print("\n# ------------------ Encryption --------------------------")
# 0. Construct a random message:
m_vec = matrix(Fqm, [Fqm.random_element() for ii in range(k-u)] + [0]*u)

# 1. Choose alpha at random
alpha = Fqmu.random_element()

# 2. Choose e such that rank_q(e) = t_pub
e_vec = rand_vec(Fqm,Fq,t_pub,n)

# 3. Build generator matrix of Gab
G_gab = matrix(Fqm,[[g[0,jj]^(q^(ii)) for jj in range(n)] for ii in range(k)])

# 4. Calculate ciphertext
c_vec = m_vec* G_gab + trace(Fqmu,Fqm,alpha * k_pub) + e_vec

print("Encryption done")

print("\n# ------------------- Decryption ---------------------------------")
# 1. Compute cP
c_prime = (c_vec*P)[0,w:]

# 2. Build generator matrix of Gab'
G_prime = (G_gab*P)[:,w:]

# 3. Decode c' ind Gab' to get m'
c_dec = Gab_decoder(G_prime,c_prime,t_pub)
m_prime = c_dec * G_prime.transpose() * (G_prime * G_prime.transpose()).inverse()

# 4. Retrieve alpha
x_dual = dual_basis(Fqmu,x_vec[0,(k-u):])
alpha_hat = sum([m_prime[0,k-u+ii]*x_dual[ii] for ii in range(u)])

# 5. Calculate m
m_hat = m_prime - trace(Fqmu,Fqm,alpha*x_vec)

print('Encryted message equals the decrypted message:')
print(m_hat == m_vec)


print("\n# ------------------- Attack ---------------------------------")

## Secret: P, s, s1, z_vec, x_vec
## Public: k_pub, g, G_gab, t_pub, k, n, w, u, Fq, Fqm, Fqmu

# 1. Compute a basis of Fqm over Fq and its dual

gamma = rand_vec(Fqmu, Fqm, rk=u, leng=u)

# assert len(gamma[0]) == u

# gamma = x_vec[0, (k-u):]

# print("\nBeware !!! I'm using the secret basis for debugging purposes\n")

gamma_dual = dual_basis(Fqmu, gamma)


# 2. Compute k_pub_coordinates in Fqm

k_pub_vec = [trace(Fqmu, Fqm, gi*k_pub).rows()[0] for gi in gamma_dual]


# 3. solve the system via RightBerlekampWelchDecoding

codewords = attack_repaired_fl(k_pub_vec, g[0], Fqm, k, w)

assert len(codewords) == u

# x_rec = [0]*n

assert all([len(codewords[i]) == n for i in range(u)])

# for j in range(n):
#     for i in range(u):
#         x_rec[j]+=codewords[i][j]*gamma[i]



c_rec = vector([sum([codewords[i][j]*gamma[0][i] for i in range(u)]) for j in range(n)])

x_init = vector(x_vec*G_gab)

assert c_rec == x_init

# print(f"Recovered {c_rec} from public key")
# print(f"Initial private key was {x_vec}")
# assert x_rec == x_pub
# for i in range(x_vec.ncols()):
#     # assert f(g[0][i]) == x_vec.row(0)[i]
#     print(f(g[0][i]))
#     print(x_vec.row(0)[i])

print("yeah !!!")

from linear_rank_metric import from_matrix_representation


def trace(Fl,Fs,vec):
    # Trace operation from a vector vec over Fl to a projection over Fs
    val_mu = log(Fl.cardinality(),Fl.characteristic())
    val_m = log(Fs.cardinality(),Fs.characteristic())
    u = val_mu / val_m
    qm = Fs.cardinality()
    output = matrix(Fl,1,vec.ncols())
    for ii in range(vec.ncols()):
        output[0,ii] = sum([vec[0,ii]^(qm^j) for j in range(u)])
    return output


def rand_vec(Fl,Fs,rk,leng):
    # Generate a random vector of length leng over Fl with rank rk over Fs
    u = Fl.degree() // Fs.degree()
    # 1. Compute echelonizable matrix with rank rk
    rank_false = true
    while rank_false:
        A_mat = matrix.random(Fs,u,rk)
        if A_mat.rank()==rk:
            rank_false = false

    rank_false = true
    while rank_false:
        B_mat = matrix.random(Fs,rk,leng)
        if B_mat.rank()==rk:
            rank_false = false

    out_mat = A_mat*B_mat
    out_vec = from_matrix_representation(out_mat, Fl)
    return out_vec

def dual_basis(Fqm, Fq, basis):
    # Generate a dual basis
    basis = vector(basis[0,:])
    m = Fqm.degree() // Fq.degree()
    entries = matrix(Fqm, [xi*xj for xi in basis for xj in basis])
    entries = (Fq^(m^2))(vector(trace(Fqm, Fq, entries)))
    B = matrix(Fq, len(basis), entries).inverse()
    db = [sum(x * y for x, y in zip(col, basis)) for col in B.columns()]
    return db

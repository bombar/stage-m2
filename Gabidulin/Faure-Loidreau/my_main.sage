reset()

from attack import attack2_repaired_fl, attack3_repaired_fl
from sage.coding.relative_finite_field_extension import RelativeFiniteFieldExtension
from sage.modules.free_module_element import vector
from gabidulin_code import GabidulinCode
from linear_rank_metric import from_matrix_representation, to_matrix_representation, rank_weight

from skew_polynomials import reduction, adjoint, vanishing_space

from copy import copy

import random

import time

load("my_helper_functions.sage")



repair = True
zeta = 1

if repair:
    print("# ----------- Repaired Version -----------------")
else:
    print("# ----------  Original Version -----------------")



# implem = "pari"
# implem = "ntl"
implem = None

#set_random_seed(1234)

p = 2
s = 1
q = p^s
# m, k, w, u = 41, 29, 9, 3  # Test, zeta = 2 et w/(n-k-w) = 5
# m, k, w, u = 41, 27, 8, 3  # Their implementation
# m, k, w, u = 61, 31, 16, 3    # Security level of 90 in the paper
# m, k, w, u = 62, 31, 17, 3  # Security level of 131.99 in the paper ---> My 'attack' leads to security level ~124 < 128
# m, k, w, u = 83, 48, 24, 4  # Security level of 256.99 in the paper ---> My 'attack' leads to security level ~249 < 256

m, k, w, u = 10, 4, 4, 3  # toy example

n = m


# Choose parameters
# Works with m=n=41, k=27
#     * u=5, w=3
#     * u=4, w=3
#     * u=3, w=4
#     * u=2, w=5
#     * u=1, w=7


# n=m=83, k=48

# u=3 --> t=28. We want w=21.
# * w=1 --> Ker(S) is of dimension 28
# * w=2 --> Ker(S) is of dimension 27
# * w=3 --> Ker(S) is of dimension 26
# * w=4 --> Ker(S) is of dimension 25
# * w=5 --> Ker(S) is of dimension 24
# * w=6 --> Ker(S) is of dimension 23
# * w=7 --> Ker(S) is of dimension 22
# * w>7 --> Ker(S) is of dimension 22 and it doesn't work




# n=m=41, k=27

# u=6 --> t=12
#  * w=1 --> Ker(S) is of dimension 12
#  * w=2 --> Ker(S) is of dimension 11
#  * w>2 --> Ker(S) is of dimension 11 but doesn't work

# u=5 --> t=11
#  * w=1 --> Ker(S) is of dimension 11
#  * w=2 --> Ker(S) is of dimension 10
#  * w=3 --> Ker(S) is of dimension 9
#  * w>3 --> Ker(S) is of dimension 9 but doesn't work

# u=4 --> t=11
#  * w=1 --> Ker(S) is of dimension 11
#  * w=2 --> Ker(S) is of dimension 10
#  * w=3 --> Ker(S) is of dimension 9
#  * w>3 --> Ker(S) is of dimension 9 but doesn't work

# u=3 --> t=10. We want w=8.
#  * w=1 --> Ker(S) is of dimension 10
#  * w=2 --> Ker(S) is of dimension 9
#  * w=3 --> Ker(S) is of dimension 8
#  * w=4 --> Ker(S) is of dimension 7
#  * w>4 --> Ker(S) is of dimension 7 but doesn't work

# u=2 --> t=9
#  * w=1 --> Ker(S) is of dimension 9
#  * w=2 --> Ker(S) is of dimension 8
#  * w=3 --> Ker(S) is of dimension 7
#  * w=4 --> Ker(S) is of dimension 6
#  * w=5 --> Ker(S) is of dimension 5
#  * w>5 --> Ker(S) is of dimension 5 but doesn't work

# u=1 --> t=7
#  * w=1 --> Ker(S) is of dimension 7
#  * w=2 --> Ker(S) is of dimension 6
#  * w=3 --> Ker(S) is of dimension 5
#  * w=4 --> Ker(S) is of dimension 4
#  * w=5 --> Ker(S) is of dimension 3
#  * w=6 --> Ker(S) is of dimension 2
#  * w=7 --> Ker(S) is of dimension 1
#  * w>7 --> Ker(S) is of dimension 1 but doesn't work


assert w < n-k
assert w > (n-k)//2
assert (n-k-w)//2 > 0


# if (n-k)<=w:
#     print("(n-k)<=w")
#     assert 0

# if w<=floor( (n-k)/2):
#     print("w<=floor( (n-k)/2)")
#     assert 0

# if floor( (n-w-k)/2 )<=0:
#     print("t_pub = 0")
#     assert 0

# Define all fields:
Fq = GF(q)
Fqm = GF(q^m, impl=implem)
Fqmu = GF(q^(m*u), impl=implem)

twist = Fqm.frobenius_endomorphism(s)


print("\n# ----------- Key Generation -------------------")
# 1. Choose g at random with rank(g)= n
g = rand_vec(Fqm,Fq,rk=n,leng=n)


# 2. Choose x at random such that the last u positions form a basis of Fqmu over Fqm
x_vec = block_matrix(Fqmu,[matrix.random(Fqmu,1,k-u),matrix(rand_vec(Fqmu,Fqm,rk=u,leng=u))],nrows=1,subdivide=False)


if repair:
    # 3. Choose s according to Section IV. Reparation of the FL System. (Here \xi is 1)
    s1 = [rand_vec(Fqm, Fq, w, w) for i in range(min(zeta, u))]
    slist = s1 + [random.choice(s1) for i in range(u-zeta)]
    # slist = [s1.]
    # s1 = rand_vec(Fqm,Fq,w,w)
    # s2 = rand_vec(Fqm,Fq,w,w)
    # s = from_matrix_representation(matrix(Fqm,[s1 for ii in range(u-1)] + [s2]))
    s = from_matrix_representation(matrix(Fqm,slist))
    print(f"zeta={zeta}. Limit for the attack to fail is {float(w/(n-k-w))}")
else:
    # 3. Choose a full rank s at random in Fqmu^w
    s = rand_vec(Fqmu, Fq, w, w)

# 4. Choose an invertible matrix P at random
P = matrix.random(Fq,n,n)
while not (P.rank() == n):
    P = matrix.random(Fq,n,n)

# 5. Build Gabidulin code
G = GabidulinCode(Fqm, n, k, Fq, twist, evaluation_points=g)
G_gab = G.generator_matrix()

# 6. Generate vector z
z_vec = block_matrix(Fqmu, [matrix(s), matrix([0]*(n-w))], nrows=1)*P.inverse()


print(f"z_vec is of rank {rank_weight(vector(z_vec))}")
print(f"s is of rank {rank_weight(vector(s))}")

print(len(vector(z_vec)))

# exit()

# 7. Generate k_pub
k_pub = x_vec*G_gab + z_vec

# 8. Compute t_pub
t_pub = floor( (n-w-k)/2 )

print("Key Generation done")

print("\n# ------------------ Encryption --------------------------")
# 0. Construct a random message:
m_vec = vector(Fqm, [Fqm.random_element() for ii in range(k-u)] + [0]*u)

# 1. Choose alpha at random
alpha = Fqmu.random_element()

# 2. Choose e such that rank_q(e) = t_pub
e_vec = rand_vec(Fqm,Fq,t_pub,n)

# 3. Build generatohar matrix of Gab
G = GabidulinCode(Fqm, n, k, Fq, twist, evaluation_points=g)
G_gab = G.generator_matrix()
E = G.encoder("VectorEvaluation")

# 4. Calculate ciphertext
trace_alpha_kpub = (Fqm^n)(vector(trace(Fqmu,Fqm,alpha * k_pub)))

c_vec = E.encode(m_vec) + trace_alpha_kpub + e_vec

print("Encryption done")

print("\n# ------------------- Decryption ---------------------------------")
# 1. Compute cP
c_prime = (c_vec*P)[w:]

# 2. Build code G'
g_prime = (g*P)[w:]
Gab_prime = GabidulinCode(Fqm, n-w, k, Fq, evaluation_points=g_prime)
G_prime = Gab_prime.generator_matrix()

# 3. Decode c' ind Gab' to get m'
print("Avant decodage")
c_dec = Gab_prime.decode_to_code(c_prime, decoder_name="BerlekampWelch")
print("Decodage done")
m_prime = Gab_prime.unencode(c_dec)
print("On récupère le message")

# 4. Retrieve alpha
x_dual = dual_basis(Fqmu, Fqm, x_vec[0,(k-u):])
alpha_hat = sum([m_prime[k-u+ii]*x_dual[ii] for ii in range(u)])

# 5. Calculate m
m_hat = m_prime - vector(trace(Fqmu,Fqm,alpha_hat*x_vec))

print('Encryted message equals the decrypted message:')
print(m_hat == m_vec)
assert m_hat == m_vec

print("\n# ------------------- Attack ---------------------------------")

start = time.time()


## Secret: P, s, s1, z_vec, x_vec
## Public: k_pub, g, G_gab, t_pub, k, n, w, u, Fq, Fqm, Fqmu

# 1. Compute a basis of Fqm over Fq and its dual

# gamma = matrix(rand_vec(Fqmu, Fqm, rk=u, leng=u))

# # gamma = x_vec[0, (k-u):]

# # print("\nBeware !!! I'm using the secret basis for debugging purposes\n")

# gamma_dual = dual_basis(Fqmu, Fqm, gamma)


# 2. Compute k_pub_coordinates in Fqm

# # 2.1 move to a close error, rank 1 over Fq AND Fqm
# zp = rand_vec(Fqm, Fq, rk=1, leng=n)
# zpu = matrix(sum([zp*g for g in gamma[0]]))
# # zpu = 0
# errp = zpu + z_vec
# verrp = vector(errp)
# zeta = rank_weight(verrp, Fqm)
# print(f"\n\nNew error is of rank {rank_weight(verrp)} over Fq and {zeta} over Fqm\n\n")

# err_vec_sec = [(Fqm^n)(vector(trace(Fqmu, Fqm, gi*errp))) for gi in gamma_dual]
# err1_rowspace = to_matrix_representation(err_vec_sec[0]).row_space()

# assert all([to_matrix_representation(err_vec_sec[i]).row_space() == err1_rowspace for i in range(u)])

# print([rank_weight(err_vec_sec[i]) for i in range(u)])


# k_pub = k_pub + zpu

# k_pub_vec = [(Fqm^n)(vector(trace(Fqmu, Fqm, gi*k_pub))) for gi in gamma_dual]

# k_pub_vec_p = [kp + zp for kp in k_pub_vec]

# z_vec_sec = [(Fqm^n)(vector(trace(Fqmu, Fqm, gi*z_vec))) for gi in gamma_dual]

# z1_rowspace = to_matrix_representation(z_vec_sec[0]).row_space()


# raise

# assert all([to_matrix_representation(z_vec_sec[i]).row_space() == z1_rowspace for i in range(u)])

# print([rank_weight(z_vec_sec[i]) for i in range(u)])



# points = [
#     [(g[j], z_vec_sec[i][j]) for j in range(n)] for i in range(u)
# ]

f = Fqm.frobenius_endomorphism(Fq.degree())
R.<y> = Fqm['y', f]

# Err_pol = [R.lagrange_polynomial(points[i]) for i in range(u)]

# Err_pol_adj = [adjoint(P, m) for P in Err_pol]

# Err_adj = [vector(P.multi_point_evaluation(g)) for P in Err_pol_adj]

# z1_adj_colspace = to_matrix_representation(Err_adj[0]).column_space()

# for i in range(u):
#     assert to_matrix_representation(Err_adj[i]).column_space() == z1_adj_colspace

# V_adj = R.minimal_vanishing_polynomial(Err_adj[0])
# V_ = adjoint(V_adj, m)

# k_pub_vec_sec = [k - z for (k, z) in zip(k_pub_vec, z_vec_sec)]

# assert all([xG in G for xG in k_pub_vec_sec])

# points_ = [[(g[i], xi) for (i, xi) in enumerate(G.unencode(xG))] for xG in k_pub_vec_sec]

# fs = [R.lagrange_polynomial(points_[i]) for i in range(u)]

# fs_star = [adjoint(f, m) for f in fs]

# Ns_coeffs = [reduction(V_adj*f, m).coefficients() for f in fs_star]

# N_s = [adjoint(reduction(V_adj*f, m),m) for f in fs_star]

# x_sec = [G.(xG) for xG in k_pub_vec_sec]


# # 3. solve the system via RightBerlekampWelchDecoding
# # raise

# t = ((u-1)*(n-k))//u

# t=22

d = (n - k - w)//2

t=None

Tk_pub = (Fqm^n)(vector(trace(Fqmu, Fqm, k_pub)))

(f, N, V) = attack3_repaired_fl(Tk_pub, c_vec, g, k, d, Fqm, q, R)

# c_rec = vector(f.multi_point_evaluation(g)) # Should be some codeword + Trace(alpha * k_pub)

# PM = R.lagrange_polynomial(list(zip(g, E.encode(m_vec))))
# PT = R.lagrange_polynomial(list(zip(g, Tk_pub)))

# trace_alpha_z = (Fqm^n)(vector(trace(Fqmu, Fqm, alpha*z_vec)))
# PZ = R.lagrange_polynomial(list(zip(g, trace_alpha_z)))

assert f.degree() == (n+k-w)//2
assert f.degree() == k + d

error_support = vanishing_space(V, Fqm, Fq)

# Check that we computed the support of the error in the ciphertext
E_matrix = to_matrix_representation(e_vec, Fq)
Priv_supp = E_matrix.column_space()

assert error_support == Priv_supp

# Now we will try to perform a syndrom decoding algorithm on Cpub = Gab + <Trace(Kpub)>

# First, we need to compute a parity-check matrix of this code.
# A generator matrix of the sum Cpub is the superpositionf of a generator matrix Mg and Tk_pub.
# Then we can just perform gaussian elimination to find a parity-check matrix.

Mg = G.generator_matrix()  # generator matrix of the Gabidulin code
Sg = block_matrix([[Mg], [matrix(Tk_pub)]])  # generator matrix of Cpub
Sg = Sg.echelon_form()  # standard form of the previous generator matrix of Cpub
H = Sg.submatrix(0, k+1, k+1, n-k-1)
H = block_matrix([[-H.transpose(), 1]])  # parity-check matrix of Cpub

synd = c_vec*H.transpose()  # syndrome

F = from_matrix_representation(error_support.basis_matrix().transpose())  # d elements in Fqm such that <e1,..., en> = <F1,...,Fd>


# From now on we can express every ei in terms of the Fj :
#
#  ei = eps_i,1*F1 + ... + eps_i,d*Fd where eps_i,j in Fq are to be found --> d*n unknowns in Fq
#
# Equations are the syndrom equations
#
# Let H^T = [H1, ..., H_(n-k-1)]  where Hj is the j-th column of H^T.
#
# Then Hj is a column of n elements in Fqm
#
#
# We then have (n-k-1) equations in Fqm :
#
# e1*H1,1 + ... + en*H1,n = s1
# .....
# e1*H_(n-k-1),1 + ... + en*H_(n-k-1),n = s_(n-k-1)
#
#
# Let's focus on the first equation :
# We may decompose it using the Fi :
#
# (eps_1,1*F1+...+eps_1,d*Fd)*H1,1 + ... + (eps_1,1*F1+...+eps_1,d*Fd)*H1,n = s1
#
# We may expand all the Fl*H1,j onto Fq. Each equation on Fqm gives m equations over Fq
#
# And we finally have (n-k-1)*m equations over Fq with n*d unknowns, which we can solve with linear algebra.
#
#
#
#

# Because I wrote t on the board ...
t = d

# We compute all the products
S = [[F[j]*H[i][l] for l in range(n) for j in range(t)] for i in range(n-k-1)]
S = [vector(v) for v in S]

# We expand everything over Fq
S = [to_matrix_representation(v, Fq) for v in S]


# We compute the matrix of the system
M = block_matrix(n-k-1, 1, S)

# We expand the syndrome over Fq
synd = to_matrix_representation(synd, Fq)

# And we compute the right member of the system
synd = matrix([[synd.transpose()[i][d]] for i in range(n-k-1) for d in range(m)])

# We solve it
eps = M.solve_right(synd).transpose()


# We can now recover the error_vector
e_rec = vector([Fqm(0)]*n)
for i in range(n):
    e_rec[i] = sum([eps[0][i*t+l]*F[l] for l in range(t)])




# We check that we recovered the right error vector
assert e_rec == e_vec


# Compute the new noisy codeword
new_c = c_vec - e_rec  # = mG + Tr(alpha*k_pub)

# Compute a basis of Fqmu over Fqm
gamma = matrix(rand_vec(Fqmu, Fqm, rk=u, leng=u))

# dual basis
gamma_dual = dual_basis(Fqmu, Fqm, gamma)

gamma = vector(gamma)
gamma_dual = vector(gamma_dual)

# Compute the traces of the gamma_i*k_pub
gk = [(Fqm^n)(vector(trace(Fqmu, Fqm, gamma[i]*k_pub))) for i in range(u)]

# Compute a parity check matrix of the Gabidulin code
Hgab = G.parity_check_matrix()

# Compute the lambda
Lambda = [gk[i]*Hgab.transpose() for i in range(u)]

# Compute the last syndromes
new_synd = new_c*Hgab.transpose()

new_M = matrix(
    Fqm,
    n-k,
    u,
    lambda i, j: Lambda[j][i]
)

Em = new_M.right_kernel()  # Dual space of xi for trace operator, dimension u-1

new_alpha_coord = new_M.solve_right(new_synd)
priv_alpha_coord = [Fqm(trace(Fqmu, Fqm, matrix(1, 1, alpha*gamma_dual[i]))[0][0]) for i in range(u)]

priv_alpha_coord = vector(priv_alpha_coord)

assert (new_alpha_coord - priv_alpha_coord) in Em

# print(f"Exhaustive search in Em of dimension {Em.rank()} over Fqm ?")
# print(f"Cardinality 2^{round(log(Em.cardinality(), 2))}")


def get_new_alpha():
    random_ksi_dual = Em.random_element()
    new_alpha = random_ksi_dual + new_alpha_coord
    new_alpha = sum(new_alpha[i]*gamma[i] for i in range(u))
    return new_alpha

def get_new_trace():
    a = get_new_alpha()
    trace_a_kpub = (Fqm^n)(vector(trace(Fqmu, Fqm, a*k_pub)))
    return trace_a_kpub


def random_m_affine():
    """
    Return a random element of the affine space
    E = m + {Trace(beta*x) | beta in the dual space of <ksi>}
      = m + {Trace(beta*x) | Trace(beta*z) = 0 }
    """
    return E.unencode(new_c - get_new_trace())




# Now we want to recover the message. We need to find a basis of E_x = {Tr(bx) | b in <ksi>^dual}
# E_x is of dimension at most u-1
# So we find u-1 vectors in E_x : For that, we just need to get 2*(u-1) random_m_affine() and then take the u-1 consecutive differences.

basis_E_x = []
for i in range(u-1):
    m1, m2 = random_m_affine(), random_m_affine()
    basis_E_x.append(matrix(m1 - m2).transpose())

basis_E_x = block_matrix(Fqm, 1, u-1, basis_E_x)

# Define Identity and zero matrices
Ik = matrix.identity(Fqm, k)
Iu = matrix.identity(Fqm, u)

Zu = matrix.zero(Fqm, nrows=u, ncols=u-1)
Zu1 = matrix.zero(Fqm, nrows=u, ncols=1)
Zk = matrix.zero(Fqm, nrows=u, ncols=k-u)

top = block_matrix([[Ik, basis_E_x]])
bot = block_matrix([[Zk, Iu, Zu]])

S = block_matrix(2, 1, [top, bot])

b = random_m_affine()
B = matrix(b).transpose()

r_member = block_matrix(2, 1, [B, Zu1])

m_rec = vector(S.solve_right(r_member).transpose())[:k]


if m_rec == m_vec:
    print("I Won !!")
else:
    print("Attack failed")

# basis = new_M.right_kernel().basis_matrix()[0]

# won = False
# for i, x in enumerate(Fqm):
#     print(i)
#     new_alpha_rec = x*basis + new_alpha_coord
#     alpha_rec = sum(new_alpha_rec[i]*gamma[i] for i in range(u))
#     if alpha_rec == alpha:
#         won = True
#         break
#     # new_trace = (Fqm^n)(vector(trace(Fqmu, Fqm, alpha_rec*k_pub)))
#     # mc_rec = new_c - new_trace
#     # # if mc_rec in G:
#     # m_rec = E.unencode(mc_rec)
#     # if not all(map(lambda x: x == 0, m_rec[k-u:])):
#     #     if m_rec == m_vec:
#     #         print("I won !")
# if won:
#     print("Attack successful !")
# else:
#     print("attack fail")

# assert mc_rec in G

# m_rec = E.unencode(mc_rec)


# if m_rec == m_vec:
#     print("I won !!")
# else:
#     print("Attack failed !")



















# print("Ok")

# raise

# codewords, S, _N, _V = attack2_repaired_fl(k_pub_vec, g, Fqm, k, w, R, t, zeta)

# (Nstar_coeff_s, Nstar_s, Ns) = _N
# (Vstar_coeff, Vstar, V) = _V


# x_rec_fqm = [G.unencode(c) for c in codewords]

# x_rec = vector([sum([x_rec_fqm[i][j]*gamma[0][i] for i in range(u)]) for j in range(k)])

# end = time.time()

# print(f"Attack lasted {round(end - start, 2)} seconds !")

# c_rec = vector([sum([codewords[i][j]*gamma[0][i] for i in range(u)]) for j in range(n)])

# x_init = vector(x_vec*G_gab)


# assert vector(x_vec) == x_rec

# print("It works !!")

# # raise

# # try:
# #     assert len(codewords) == u

# #     # x_rec = [0]*n

# #     assert all([len(codewords[i]) == n for i in range(u)])

# #     # for j in range(n):
# #     #     for i in range(u):
# #     #         x_rec[j]+=codewords[i][j]*gamma[i]



# #     c_rec = vector([sum([codewords[i][j]*gamma[0][i] for i in range(u)]) for j in range(n)])

# #     x_init = vector(x_vec*G_gab)

# #     assert c_rec == x_init

# #     # print(f"Recovered {c_rec} from public key")
# #     # print(f"Initial private key was {x_vec}")
# #     # assert x_rec == x_pub
# #     # for i in range(x_vec.ncols()):
# #     #     # assert f(g[0][i]) == x_vec.row(0)[i]
# #     #     print(f(g[0][i]))
# #     #     print(x_vec.row(0)[i])

# #     print("yeah !!!")
# # except TypeError:
# #     print("Error")

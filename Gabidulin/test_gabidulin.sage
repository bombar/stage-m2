
from gabidulin_code import GabidulinCode
from rank_channel import StaticRankErrorChannel


p = 2
s = 1
q = p^s
m = 82
n = m
k = 63



print(f"q={p}^{s}, m={m}, n={n}, k={k}\n")


Fqm = GF(q^m)
Fq = GF(q)
twist = Fqm.frobenius_endomorphism(s)


# [n, k, n-k+1] linear Gabidulin code over GF(q^m)/GF(q)")
G = GabidulinCode(Fqm, n, k, Fq, twist)
print(f"[{n}, {k}, {n-k+1}] linear Gabidulin code over GF({q}^{m})/GF({q})")

# Base subfield
Fq = G.sub_field()
print(f"Fq = {Fq}")

# decoding radius = (d-1)//2
t = (G.minimum_distance()-1)//2
print(f"decoding radius = {t}\n")


print("Generating random codeword")
c = G.random_element()

# Rank error channel
Chan = StaticRankErrorChannel(G.ambient_space(), t, G.sub_field())
print(f"and transmitting it through {Chan}")
y = Chan(c)

E = G.encoder("PolynomialEvaluation")

f = E.unencode_nocheck(c)
S = E.message_space()

print(f"\nInput polynomial is\n\nf={f}\n")

rank_error = G.rank_weight_of_vector(y-c)


print(f"received noisy word with error of rank {rank_error}.")

assert(rank_error <= t)

decoder = "RightBerlekampWelch"
# decoder = "BerlekampWelch"
# decoder = "Gao"
print(f"\nInstantiating {decoder} Decoder")
D = G.decoder(decoder)



print("Trying to decode ...\n")
d, Y = D._decode_to_code_and_message(y)

assert(d == c)

print(f"\nDecoded polynomial is\n\nf={Y}")


print("\n\n... Done !")

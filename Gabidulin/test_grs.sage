
from sage.coding.grs_code import GeneralizedReedSolomonCode
from sage.coding.channel import StaticErrorRateChannel


p = 3
s = 1
q = p^s
m = 5
n = m
k = 2



print(f"q={p}^{s}, m={m}, n={n}, k={k}\n")


Fqm = GF(q^m)
Fq = GF(q)
twist = Fqm.frobenius_endomorphism(s)


C = GeneralizedReedSolomonCode(Fqm.list()[:n], k)

print(C)

c = C.random_element()

# decoding radius = (d-1)//2
t = (C.minimum_distance()-1)//2
print(f"decoding radius = {t}\n")


print("Generating random codeword")
c = C.random_element()

# Rank error channel
Chan = StaticErrorRateChannel(C.ambient_space(), t)
print(f"and transmitting it through {Chan}")
y = Chan(c)

nberror = (y-c).hamming_weight()

print(f"received noisy word with {nberror} errors.")

assert(nberror <= t)

decoder = "BerlekampWelch"
# decoder = "Gao"
print(f"\nInstantiating {decoder} Decoder")
D = C.decoder(decoder)



print("Trying to decode ...\n")
d = D.decode_to_code(y)
assert(d == c)
print("\n\n... Done !")
